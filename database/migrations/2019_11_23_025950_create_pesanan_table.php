<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_pemesan',50);
            $table->string('telepon',15);
            $table->string('alamat_kirim');
            $table->string('email',100);
            $table->dateTime('tgl_pemesanan');
            $table->dateTime('tgl_selesai');
            $table->bigInteger('total_harga');
            $table->string('keterangan');
            $table->string('status_pemesanan',50);
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan');
    }
}
