<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    //
    protected $table = 'admin';
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'alamat',
        'foto',
        'telepon',
    ];
}
