<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bahan extends Model
{
    //
    protected $table='bahan';
    protected $fillable = [
        'nama',
        'keterangan'
    ];

    public function produk()
    {
        return $this->belongsToMany('App\Produk');
    }

    public function detail_pemesanan()
    {
        return $this->hasMany(Detail_Pemesanan::class);
    }
}
