<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Produk;

class Ukuran extends Model
{
    //
    protected $table = "ukuran";
    protected $fillable = ['produk_id','dimensi','s','m','l','xl','xxl','xxxl'];

    public function produk()
    {
        return $this->belongsTo(Produk::class);
    }
}
