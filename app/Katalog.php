<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Katalog extends Model
{
    protected $table = 'katalog';
    protected $fillable = ['keterangan','foto'];

    public function produk()
    {
        return $this->belongsTo(Katalog::class);
    }
}
