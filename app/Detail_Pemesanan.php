<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Pemesanan extends Model
{
    //
    protected $table = 'detail_pesanan';
    protected $fillable = [
        'pesanan_id',
        'produk_id',
        'bahan_id',
        'jumlah_ukuran',
        'total_pesan',
        'sub_harga',
        'desain'
    ];

    public function pesanan()
    {
        return $this->belongsTo(Pesanan::class);
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class);
    }

    public function bahan()
    {
        return $this->belongsTo(Bahan::class);
    }
}
