<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ukuran;

class Produk extends Model
{
    //
    protected $table = 'produk';
    protected $fillable = [
        'nama',
        'gambar_produk'
    ];

    public function katalog()
    {
        return $this->hasMany(Katalog::class);
    }

    public function bahan()
    {
        return $this->belongsToMany('App\Bahan');
    }

    public function detail_pemesanan()
    {
        return $this->hasMany('App\Detail_Pemesanan');
    }

    public function ukuran()
    {
        return $this->hasMany(Ukuran::class);
    }
}
