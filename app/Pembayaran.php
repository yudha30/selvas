<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pesanan;
use App\Detail_Pembarayan;

class Pembayaran extends Model
{
    //
    protected $table = 'pembayaran';
    protected $fillable = [
        'pesanan_id',
        'jenis_pembayaran',
        'total_bayar',
        'status_pembayaran',
    ];

    public function pesanan()
    {
        return $this->belongsTo(Pesanan::class);
    }

    public function detail_pembayaran()
    {
        return $this->hasMany(Detail_Pembayaran::class);
    }
}
