<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Detail_Pemesanan;
use App\Pembayaran;

class Pesanan extends Model
{
    protected $table = 'pesanan';
    protected $fillable = [
        'nama_pemesan',
        'telepon',
        'alamat_kirim',
        'email',
        'tgl_pemesanan',
        'tgl_selesai',
        'total_harga',
        'keterangan',
        'status_pemesanan'
    ];

    public function detail_pemesanan()
    {
        return $this->hasMany(Detail_Pemesanan::class);
    }

    public function pembayaran()
    {
        return $this->hasOne(Pembayaran::class);
    }
}
