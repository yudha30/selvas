<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bahan_Produk extends Model
{
    //
    protected $table = 'bahan_produk';
    protected $fillable = [
        'bahan_id' ,
        'produk_id'
    ];
}
