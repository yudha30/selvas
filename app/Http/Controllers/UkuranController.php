<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ukuran;
use App\Produk;

class UkuranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $produk=Produk::all();
        $data=Ukuran::paginate(15);
        return view('admin/ukuran',compact('data','produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'produk' => 'required',
            'dimensi' => 'required',
            'S' => 'required|max:3',
            'M' => 'required|max:3',
            'L' => 'required|max:3',
            'XL' => 'required|max:3',
            'XXL' => 'required|max:3',
            'XXXL' => 'required|max:3',
        ]);
        $ukuran = Ukuran::create([
            'produk_id' => $request->produk,
            'dimensi' => $request->dimensi,
            's' => $request->S,
            'm' => $request->M,
            'l' => $request->L,
            'xl' => $request->XL,
            'xxl' => $request->XXL,
            'xxxl' => $request->XXXL,
        ]);

        return redirect('/admin/ukuran')->with('sukses','Berhasil Menambahkan Ukuran!');
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->XXXL);
        $validatedData = $request->validate([
            'produk' => 'required',
            'dimensi' => 'required',
            'S' => 'required|max:3',
            'M' => 'required|max:3',
            'L' => 'required|max:3',
            'XL' => 'required|max:3',
            'XXL' => 'required|max:3',
            'XXXL' => 'required|max:3',
        ]);
        $ukuran = Ukuran::where('id',$id)->update([
            'produk_id' => $request->produk,
            'dimensi' => $request->dimensi,
            's' => $request->S,
            'm' => $request->M,
            'l' => $request->L,
            'xl' => $request->XL,
            'xxl' => $request->XXL,
            'xxxl' => $request->XXXL
        ]);

        return redirect('/admin/ukuran')->with('sukses','Berhasil Ubah Ukuran!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Ukuran::find($id)->delete();
        return redirect('/admin/ukuran')->with('sukses','Ukuran Berhasil Dihapus');
    }
}
