<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use Illuminate\Support\Facades\DB;
use File;
use App\Katalog;
use App\Bahan_Produk;

class KaosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $data=DB::table('katalog')->where('produk_id',1)->get();
        $produk=Produk::where('id',1)->get();

        return view('admin.katalog', compact('data','produk'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $getProduk=Produk::find($request->produk);
        // dd($request->id);
        $validatedData = $request->validate([
            'keterangan' => 'required|max:255',
            'foto' => 'required|image|max:2048'
        ]);
        if($request->hasFile('foto')){
            $request->file('foto')->move('admin/katalog/'.strtolower($getProduk->nama_produk).'/',date("dmYhis").$request->file('foto')->getClientOriginalName());
            $foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
        }
        DB::table('katalog')->insert([
            'produk_id' => $request->produk,
            'keterangan' => $request->keterangan,
            'foto' => $foto
        ]);
        return redirect('/admin/daftarkatalog/'.$getProduk->id)->with('sukses','Data Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kd)
    {
        //
        $gambar = Katalog::where('kd_katalog',$kd)->first();
        $getProduk=Produk::find($gambar->produk_id);
        File::delete('admin/katalog/'.strtolower($getProduk->nama_produk).'/'.$gambar->foto);
        DB::table('katalog')->where('kd_katalog', $kd)->delete();
        return redirect('/admin/daftarkatalog/'.$getProduk->id)->with('sukses','Data Berhasil Dihapus!');
    }
}
