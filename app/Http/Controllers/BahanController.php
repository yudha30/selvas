<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bahan;
use Illuminate\Support\Facades\DB;

class BahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=Bahan::all();
        return view('admin.bahan',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'nama' => 'required',
            'keterangan' => 'required'
        ]);

        $data = new Bahan;
        $data->insert([
            'nama_bahan' => $request->nama,
            'keterangan' => $request->keterangan
        ]);
        return redirect('admin/bahan')->with('sukses','Data Berhasil Ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'nama' => 'required',
            'keterangan' => 'required'
        ]);


        DB::table('bahan')->where('id', $id)->update([
            'nama_bahan' => $request->nama,
            'keterangan' => $request->keterangan
        ]);

        return redirect('admin/bahan')->with('sukses','Data Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // dd($id);
        DB::table('bahan')->where('id',$id)->delete();
        DB::table('bahan_produk')->where('bahan_id',$id)->delete();
        return redirect('admin/bahan')->with('sukses','Data Berhasil Dihapus!');
    }
}
