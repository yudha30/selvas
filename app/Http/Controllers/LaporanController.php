<?php

namespace App\Http\Controllers;
use App\Pesanan;
Use App\Pembayaran;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    //
    public function laporan()
    {
        $bulan="";$tahun="";
        $relasi=DB::table('pembayaran')
        ->join('pesanan','pesanan.id','=','pembayaran.pesanan_id')
        ->select('pesanan.nama_pemesan','pembayaran.pesanan_id','pesanan.status_pemesanan','pesanan.total_harga','pembayaran.status_pembayaran','pembayaran.total_bayar','pesanan.tgl_selesai','pembayaran.created_at')
        ->latest()
        ->paginate(50);

        return view('admin.laporan',compact('relasi','bulan','tahun'));
    }

    public function cari(Request $request)
    {
        // dd($request->bulan);
        // $validatedData = $request->validate([
        //     'tahun' => 'required',
        //     'bulan' => 'required'
        // ]);
        $bulan="";$tahun="";
        // if($request->bulan=="00")
        // {
        //     return redirect('/admin/laporan')->with('gagal','Terdapat kesalahan pada pencarian');
        // }
        // else{
            $relasi=DB::table('pembayaran')
            ->join('pesanan','pesanan.id','=','pembayaran.pesanan_id')
            ->select('pesanan.nama_pemesan','pembayaran.pesanan_id','pesanan.status_pemesanan','pesanan.total_harga','pembayaran.status_pembayaran','pembayaran.total_bayar','pesanan.tgl_selesai')
            ->whereBetween('pesanan.tgl_pemesanan', [$request->bulan, $request->tahun])
            ->paginate(1000);
            // dd($relasi);
            $cek=0;
            foreach($relasi as $r)
            {
                $cek++;
            }
            if($cek==0)
            {
                return redirect('/admin/laporan')->with('gagal','Data tidak ditemukan.');
            }
            else{
                $bulan=$request->bulan;
                $tahun=$request->tahun;
                return view('admin.laporan',compact('relasi','bulan','tahun'));
            }
        // }
    }

    public function cetak(Request $request)
    {
        if($request->bulan=="" && $request->tahun=="")
        {
            return redirect('/admin/laporan')->with('gagal','Belum memilih rentang waktu.');
        }
        $bulan=$request->bulan;
        $tahun=$request->tahun;
        $relasi=DB::table('pembayaran')
            ->join('pesanan','pesanan.id','=','pembayaran.pesanan_id')
            ->select('pesanan.nama_pemesan','pembayaran.pesanan_id','pesanan.status_pemesanan','pesanan.total_harga','pembayaran.status_pembayaran','pembayaran.total_bayar','pesanan.tgl_selesai')
            ->whereBetween('pesanan.tgl_pemesanan', [$request->bulan, $request->tahun])
            ->get();

            $pdf = PDF::loadview('admin.cetak_laporan',compact('relasi','bulan','tahun'));
    	    return $pdf->download('laporan-'.$request->bulan.'-'.$request->tahun);
    }
}
