<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Pelanggan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use File;
use Livewire\WithPagination;

class AdminController extends Controller
{
    use WithPagination;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index(Request $request)
    {
        //
        if($request->has('cari')){
            $cari = Admin::where('name', 'like', '%'.$request->cari.'%')
            ->orWhere('email', 'like', '%'.$request->cari.'%')
            ->paginate(5);

            return view('admin.karyawan',['admin'=>$cari]);
        }
        else{
            $admin = Admin::paginate(5);
            return view('admin.karyawan',['admin'=>$admin]);
        }

        // return view('admin.karyawan',['admin'=>$admin]);
    }


    public function unduh(Admin $admin){
        $file = public_path() . '/admin/foto_admin/' . $admin->foto;//Mencari file dari storage htdocs
        return response()->download($file, $admin->foto); //Download file yang dicari berdasarkan nama file
        return redirect('/admin/karyawan');
   }


    public function pelanggan()
    {
        $pelanggan=Pelanggan::all();
        return view('admin.pelanggan',['pelanggan'=>$pelanggan]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:admin|email',
            'password' => 'required|min:8',
            'alamat' => 'required',
            'foto' => 'image|max:2048',
            'telepon' => 'required|numeric',
        ]);
        $foto="default.jpg";
        $admin = Admin::where('email', $request->email)->first();
        if($admin){
            return redirect('/admin/karyawan')->with('gagal','Gagal Disimpan. Email Sudah Terpakai!');
        }
        else{
            if($request->hasFile('foto')){
                $request->file('foto')->move('admin/foto_admin/',date("dmYhis").$request->file('foto')->getClientOriginalName());
                $foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
            }

            $data = new Admin;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->password = Hash::make($request->password);
            $data->alamat = $request->alamat;
            $data->foto = $foto;
            $data->telepon = $request->telepon;
            $data->save();

            return redirect('/admin/karyawan')->with('sukses','Data Berhasil Ditambahkan!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        // $admin = Admin::find($admin->$id);
        // dd($admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'alamat' => 'required',
            'telepon' => 'required|numeric',
            'foto' => 'image|max:2048'
            ]);
            // hapus file
            $gambar = Admin::where('id',$admin->id)->first();
            $masuk=Admin::find($request->id);
        if($request->hasFile('foto')){
            File::delete('admin/foto_admin/'.$gambar->foto);
            $request->file('foto')->move('admin/foto_admin/',date("dmYhis").$request->file('foto')->getClientOriginalName());
            $masuk->foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
            $masuk->save();
        }

        $data = Admin::find($admin->id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->alamat = $request->alamat;
        $data->telepon = $request->telepon;
        $data->save();

        return redirect('/admin/karyawan')->with('sukses','Data Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        // hapus file
        $gambar = Admin::where('id',$admin->id)->first();
        File::delete('admin/foto_admin/'.$gambar->foto);
        Admin::destroy($admin->id);
        return redirect('/admin/karyawan')->with('sukses','Data Berhasil Dihapus!');
    }
}
