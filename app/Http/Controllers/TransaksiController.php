<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\Detail_Pembayaran;
use Illuminate\Support\Facades\DB;
use App\Events\FormNotification;
use App\Events\NotificationPembayaran;
use App\Pembayaran;
use Auth;
use File;
use PDF;
use Midtrans_Config;
use Midtrans_Snap;
use Midtrans_Notification;

class TransaksiController extends Controller
{
    //
    protected $request;
    protected $snapToken;
    public function __construct(Request $request)
    {
        $this->request = $request;
        \Midtrans\Config::$serverKey = env('MIDTRANS_SERVERKEY');
        \Midtrans\Config::$clientKey = env('MIDTRANS_CLIENTKEY');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = env('MIDTRANS_IS_PRODUCTION', false);
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = env('MIDTRANS_IS_SANITIZED', true);
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = env('MIDTRANS_IS_3DS', true);
    }

    public function submitBank()
    {
        \DB::transaction(function(){
            $getIdDetailPembayaran=Detail_Pembayaran::select('id')
            ->orderBy('id', 'desc')
            ->first();
            $getIdPembayaran=Pembayaran::where('pesanan_id',$this->request->id_pesan)->first();
            $idDetailPembayaran=0;
            if(!$getIdPembayaran){
                $idPembayaran=Pembayaran::select('id')->orderBy('id', 'desc')->first();
                $id=intval($idPembayaran->id)+1;
                if($getIdDetailPembayaran)
                {
                    $idDetailPembayaran=intval($getIdDetailPembayaran->id)+1;
                    $idDetailPembayaran='SELVAS-'.$id.'-'.$idDetailPembayaran;
                }
                else{
                    $idDetailPembayaran=1;
                    $idDetailPembayaran='SELVAS-'.$id.'-'.$idDetailPembayaran;
                }
            }else{
                if($getIdDetailPembayaran)
                {
                    $idDetailPembayaran=intval($getIdDetailPembayaran->id)+1;
                    $idDetailPembayaran='SELVAS-'.$getIdPembayaran->id.'-'.$idDetailPembayaran;
                }
                else{
                    $idDetailPembayaran=1;
                    $idDetailPembayaran='SELVAS-'.$getIdPembayaran->id.'-'.$idDetailPembayaran;
                }
            }

            $cek = Pembayaran::where('pesanan_id',$this->request->id_pesan)->first();
            if(!$cek)
            {
                $Pembayaran = new Pembayaran;
                $Pembayaran->pesanan_id = $this->request->id_pesan;
                $Pembayaran->total_bayar = 0;
                $Pembayaran->status_pembayaran = 'pending';
                $Pembayaran->jenis_pembayaran = 'DP';
                $Pembayaran->created_at = date('Y-m-d H:i:s');
                $Pembayaran->updated_at = date('Y-m-d H:i:s');
                $Pembayaran->save();
            }

            $params = array(
                'transaction_details' => array(
                    'order_id' => $idDetailPembayaran,
                    'gross_amount' => $this->request->dp,
                ),
                'customer_details'=> array(
                    'first_name'=> $this->request->nama,
                    'last_name'=> '',
                    'email'=> $this->request->email,
                    'phone'=> $this->request->telepon,

                )
            );
            $snapToken = \Midtrans\Snap::getSnapToken($params);
            // Beri response snap token
            $this->response['snap_token'] = $snapToken;
        });
        return response()->json($this->response);
    }

    /**
     * Midtrans notification handler.
     *
     * @param Request $request
     *
     * @return void
     */
    public function notificationHandler(Request $request)
    {
        $notif = new \Midtrans\Notification();
        \DB::transaction(function() use($notif) {
        $transaction_id=$notif->transaction_id;
        $gross_amount=$notif->gross_amount;
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $payment_type=$notif->payment_type;
        $order=explode('-',$order_id);
        $getIdDetailPembayaran=$order[2];
        $getIdPembayaran=$order[1];
        $fraud = $notif->fraud_status;

          if ($transaction == 'capture') {

            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {

              if($fraud == 'challenge') {
                // TODO set payment status in merchant's database to 'Challenge by FDS'
                // TODO merchant should decide whether this transaction is authorized or not in MAP
                $DetailPembayaran=new Detail_Pembayaran;
                $DetailPembayaran->id=$getIdDetailPembayaran;
                $DetailPembayaran->pembayaran_id=$getIdPembayaran;
                $DetailPembayaran->tipe_pembayaran=$payment_type;
                $DetailPembayaran->jumlah_pembayaran=$gross_amount;
                $DetailPembayaran->status_pembayaran=$transaction;
                $DetailPembayaran->snap_token=$transaction_id;
                $DetailPembayaran->save();

                $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
                $bayar->setPending();
              } else {
                // TODO set payment status in merchant's database to 'Success'
                $Pembayaran=Pembayaran::find($getIdPembayaran);
                $DetailPembayaran=Detail_Pembayaran::find($getIdDetailPembayaran);
                $add=intval($DetailPembayaran->jumlah_pembayaran)+intval($Pembayaran->total_bayar);
                $CekLunas=intval($Pembayaran->pesanan->total_harga)-$add;
                if($CekLunas==0)
                {
                    $UpdateStatusPembayaran=Pembayaran::find($getIdPembayaran)
                        ->update([
                            'total_bayar'=>$add,
                            'status_pembayaran'=>'Lunas'
                        ]);
                }else{
                    $UpdateStatusPembayaran=Pembayaran::find($getIdPembayaran)
                        ->update([
                            'total_bayar'=>$add,
                            'status_pembayaran'=>'Belum Lunas'
                        ]);
                }
                $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
                $bayar->setSuccess();
                $text='Ada Pembayaran Baru. Silahkan Cek Pembayaran!';
                event(new NotificationPembayaran($text));
              }

            }

          } elseif ($transaction == 'settlement') {

            // TODO set payment status in merchant's database to 'Settlement'
            $Pembayaran=Pembayaran::find($getIdPembayaran);
            $DetailPembayaran=Detail_Pembayaran::find($getIdDetailPembayaran);
            $add=intval($DetailPembayaran->jumlah_pembayaran)+intval($Pembayaran->total_bayar);
            $CekLunas=$Pembayaran->pesanan->total_harga-$add;
            if($CekLunas==0){
                $UpdateStatusPembayaran=Pembayaran::find($getIdPembayaran)
                    ->update([
                        'total_bayar'=>$add,
                        'status_pembayaran'=>'Lunas'
                    ]);

            }
            else{
                $UpdateStatusPembayaran=Pembayaran::find($getIdPembayaran)
                    ->update([
                        'total_bayar'=>$add,
                        'status_pembayaran'=>'Belum Lunas'
                    ]);
            }

            $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
            $bayar->setSuccess();
            $text='Ada Pembayaran Baru. Silahkan Cek Pembayaran!';
            event(new NotificationPembayaran($text));

          } elseif($transaction == 'pending'){

            // TODO set payment status in merchant's database to 'Pending'
            $DetailPembayaran=new Detail_Pembayaran;
            $DetailPembayaran->id=$getIdDetailPembayaran;
            $DetailPembayaran->pembayaran_id=$getIdPembayaran;
            $DetailPembayaran->jumlah_pembayaran=$gross_amount;
            $DetailPembayaran->tipe_pembayaran=$payment_type;
            $DetailPembayaran->status_pembayaran=$transaction;
            $DetailPembayaran->snap_token=$transaction_id;
            $DetailPembayaran->save();

            $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
            $bayar->setPending();

          } elseif ($transaction == 'deny') {

            // TODO set payment status in merchant's database to 'Failed'
            $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
            $bayar->setFailed();

          } elseif ($transaction == 'expire') {

            // TODO set payment status in merchant's database to 'expire'
            $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
            $bayar->setExpired();

          } elseif ($transaction == 'cancel') {

            // TODO set payment status in merchant's database to 'Failed'
            $bayar = Detail_Pembayaran::findOrFail($getIdDetailPembayaran);
            $bayar->setFailed();

          }

        });
        return;
    }


    public function notif($id)
    {
        $text='Ada Pemesanan Baru. Silahkan Cek Pesanan!';
        event(new FormNotification($text));

        DB::table('pesanan')->where('id',$id)
        ->update([
            'status_pemesanan'=>'Pending'
        ]);

        return redirect('/transaksi');

    }

    public function notif_pembayaran()
    {
        $text='Ada Pembayaran Baru. Silahkan Cek Pembayaran!';
        event(new NotificationPembayaran($text));
        return redirect('/transaksi')->with('sukses','Berhasil upload pembayaran.');
    }

    public function lihat_transaksi()
    {
        $email=Auth::user()->email;
        $pesan=Pesanan::where('email',$email)
        ->where('status_pemesanan','!=','baru')->where('status_pemesanan','!=','selesai')
        ->orderByDesc('id')
        ->get();
        $cek=count($pesan);
        if($cek == 0)
        {
            return redirect('/pesan')->with('gagal','Anda tidak memiliki transaksi.');
        }
        $DataTransaksi=array();
        $bayar=array();
        $total_bayar=array();
        $status_bayar = array();
        $status_pesanan = array();
        foreach ($pesan as $pesan) {
            array_push($DataTransaksi,$pesan);

            if($pesan->status_pemesanan!="Baru"){
                if($pesan->status_pemesanan!="Selesai"){
                    $relasi=array();
                    $data=DB::table('detail_pesanan')
                    ->join('produk','produk.id','=','detail_pesanan.produk_id')
                    ->join('bahan','bahan.id','=','detail_pesanan.bahan_id')
                    ->join('pesanan','pesanan.id','=', 'detail_pesanan.pesanan_id')
                    ->select('detail_pesanan.pesanan_id','produk.nama_produk','bahan.nama_bahan','detail_pesanan.jumlah_ukuran','detail_pesanan.total_pesan','pesanan.tgl_selesai','detail_pesanan.sub_harga','pesanan.status_pemesanan')
                    ->where('pesanan.email',$email)
                    ->where('status_pemesanan','!=','baru')
                    ->where('status_pemesanan','!=','selesai')
                    ->get();
                    foreach($data as $data){
                        array_push($relasi,$data);
                    }
                    $Pembayaran=Pembayaran::where('pesanan_id',$pesan->id)->first();
                    $JumlahBayar=0;

                    if(!$Pembayaran)
                    {
                        array_push($total_bayar,$JumlahBayar);
                        array_push($status_bayar,"Menunggu Pembayaran");
                    }
                    else{
                        $JumlahBayar=$JumlahBayar+$Pembayaran->total_bayar;
                        array_push($status_bayar,$Pembayaran->status_pembayaran);
                        array_push($total_bayar,$Pembayaran->total_bayar);
                    }

                    $nama=$pesan->nama_pemesan;
                    $telepon=$pesan->telepon;
                    $email=$pesan->email;
                    $alamat=$pesan->alamat_kirim;
                    $id=$pesan->id;
                    array_push($status_pesanan,$pesan->status_pemesanan);
                }
            }
        }
        return view('transaksi.transaksi',compact('DataTransaksi','status_bayar','total_bayar','relasi','id','status_pesanan','nama','telepon','email','alamat'));
    }

    public function cetak_transaksi($idPesan)
    {
        $email=Auth::user()->email;
        $pesan=Pesanan::where('email',$email)
        ->where('status_pemesanan','!=','baru')->where('status_pemesanan','!=','selesai')
        ->first();
        if(!$pesan)
        {
            return redirect('/pesan')->with('gagal','Anda tidak memiliki transaksi.');
        }
        elseif($pesan->status_pemesanan=="Baru" or $pesan->status_pemesanan=="Selesai"){
            return redirect('/pesan')->with('gagal','Anda tidak memiliki transaksi.');
        }
        else{
            $relasi=DB::table('detail_pesanan')
            ->join('produk','produk.id','=','detail_pesanan.produk_id')
            ->join('bahan','bahan.id','=','detail_pesanan.bahan_id')
            ->join('pesanan','pesanan.id','=', 'detail_pesanan.pesanan_id')
            ->select('detail_pesanan.pesanan_id','produk.nama_produk','bahan.nama_bahan','detail_pesanan.jumlah_ukuran','detail_pesanan.total_pesan','pesanan.tgl_selesai','detail_pesanan.sub_harga','pesanan.status_pemesanan')
            ->where('pesanan.email',$email)
            ->where('status_pemesanan','!=','baru')
            ->where('status_pemesanan','!=','selesai')
            ->where('pesanan.id',$idPesan)
            ->get();
            $nama=$pesan->nama_pemesan;
            $telepon=$pesan->telepon;
            $email=$pesan->email;
            $alamat=$pesan->alamat_kirim;
            $id=$pesan->id;
            $bayar=Pembayaran::where('pesanan_id',$pesan->id)->first();
            $total_bayar;
            if(!$bayar)
            {
                $total_bayar=0;
                $status_bayar="Menunggu Pembayaran";
            }
            else{
                $status_bayar=$bayar->status_pembayaran;
                $total_bayar=$bayar->total_bayar;
            }
            $pdf = PDF::loadview('pesan.cetak_transaksi',compact('status_bayar','total_bayar','relasi','id','pesan','nama','telepon','email','alamat'));
    	    return $pdf->download('Transaksi-Selvas-'.$nama.'.pdf');
        }
    }

    public function upload_pembayaran(Request $request,$id)
    {
        $validatedData = $request->validate([
            'bayar' => 'required|file|image',
        ]);
        $pembayaran=Pembayaran::where('pesanan_id',$id)->first();
        if(!$pembayaran)
        {
            $bayar="";
            if($request->hasFile('bayar')){
                $request->file('bayar')->move('admin/bukti_pembayaran/',date("dmYhis.").$request->file('bayar')->getClientOriginalExtension());
                $bayar=date("dmYhis.").$request->file('bayar')->getClientOriginalExtension();
            }
            DB::table('pembayaran')->insert([
                'pesanan_id'=>$id,
                'status_pembayaran'=>'Menunggu Konfirmasi',
                'bukti_pembayaran'=>$bayar,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
            return redirect('/notif_pembayaran');
        }
        else
        {
            $bayar="";
            if($request->hasFile('bayar')){
                File::delete('admin/bukti_pembayaran/'.$pembayaran->bukti_pembayaran);
                $request->file('bayar')->move('admin/bukti_pembayaran/',date("dmYhis.").$request->file('bayar')->getClientOriginalExtension());
                $bayar=date("dmYhis.").$request->file('bayar')->getClientOriginalExtension();
            }
            DB::table('pembayaran')->where('pesanan_id', $id)->update([
                'bukti_pembayaran'=>$bayar,
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
            return redirect('/notif_pembayaran');
        }
    }
}
