<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use App\Admin;

class LupaPasswordController extends Controller
{
    //

    public function tampilubah()
    {
        return view('auth/lupa_password');
    }

    public function update(Request $request, Admin $admin)
    {
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8'
        ]);
        $cek=Admin::where('email', $request->email)->first();
        // dd($cek);
        if($cek)
        {
            Admin::where('email', $cek->email)->update([

                'password' => Hash::make($request->password)

            ]);
        }
        return redirect('admin/karyawan')->with('sukses','Password Berhasil Diubah.');
    }
}
