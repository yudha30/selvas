<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\Pembayaran;
use App\Pelanggan;
use App\Produk;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $pesanan=Pesanan::all();
        $hitung_pesanan=$pesanan->count();
        $pembayaran=Pembayaran::all();
        $hitung_pembayaran=$pembayaran->count();
        $pelanggan=Pelanggan::all();
        $hitung_pelanggan=$pelanggan->count();
        $produk=Produk::all();
        $hitung_produk=$produk->count();
        // dd($hitung_pesanan);
        return view('admin.dashboard',compact('hitung_pesanan','hitung_pembayaran','hitung_pelanggan','hitung_produk'));
    }
}
