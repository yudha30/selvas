<?php

namespace App\Http\Controllers;

use App\Pembayaran;
use App\Pesanan;
use App\Detail_Pembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\NotifPembayaran;
use Livewire\WithPagination;

class PembayaranController extends Controller
{
    use WithPagination;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $pembayaran=Pembayaran::latest()->paginate(30);
        return view('admin.pembayaran',compact('pembayaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->jml_bayar);
        $bayar = Pembayaran::find($id);
        $bayar->jumlah_pembayaran = $request->jml_bayar;
        $bayar->status_pembayaran = $request->status;
        $bayar->updated_at=date('Y-m-d H:i:s');
        $bayar->save();
        // =============== kirim notif pembayaran =============
        // $data=Pesanan::where('id',$id)->first();
        $tampung_pesanan=$request->status;
        $tampung_email=$bayar->pesanan->email;
        $data_pesanan=array(
            'email_body'=> 'Status Pembayaran Anda = '.$tampung_pesanan,
        );

        \Mail::to($tampung_email)->send(new NotifPembayaran($tampung_pesanan));
        return redirect('/admin/pembayaran')->with('sukses','Data berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pembayaran::destroy($id);
        return redirect('/admin/pembayaran')->with('sukses', 'Data pembayaran berhasil dihapus');
    }

    public function cari(Request $request)
    {
        // dd($request->cari);
        $pembayaran=Pembayaran::where('pesanan_id', 'like', '%'.$request->cari.'%')
        ->paginate(10);
        $cek=0;
        foreach($pembayaran as $p)
        {
            $cek++;
        }
        if($cek==0)
        {
            return redirect('/admin/pembayaran')->with('gagal','Data tidak ditemukan');
        }
        else{
            return view('admin.pembayaran',compact('pembayaran'));
        }
    }

    public function cari_pesanan($id)
    {
        $data=Pesanan::where('id',$id)->paginate(15);
        return view('admin.pesanan',compact('data'));
    }

    public function caritanggal($tgl)
    {
        $pembayaran=Pembayaran::whereDate('updated_at',$tgl)->paginate(0);

        return view('admin.pembayaran',compact('pembayaran'));
    }

    public function getDetailPembayaran($idPembayaran){
        $getDetail=Detail_Pembayaran::where('pembayaran_id',$idPembayaran)->get();
        $iterasi=0;
        $cek=count($getDetail);
        if(!$cek){
            return "Tidak Ada Detail Pembayaran";
        }
        foreach($getDetail as $getDetail)
        {
            if($iterasi==0){
                echo "<div class='form-group row'>
            <div class='col-sm-4'>Tipe Pembayaran</div>
            <div class='col-sm-1'>:</div>
            <div class='col-sm-7'>
                $getDetail->tipe_pembayaran
            </div>
        </div>
        <div class='form-group row'>
            <div class='col-sm-4'>Jumlah Pembayaran</div>
            <div class='col-sm-1'>:</div>
            <div class='col-sm-7'>
                $getDetail->jumlah_pembayaran
            </div>
        </div>
        <div class='form-group row'>
            <div class='col-sm-4'>Status Pembayaran</div>
            <div class='col-sm-1'>:</div>
            <div class='col-sm-7'>
                $getDetail->status_pembayaran
            </div>
        </div>";
            }
            else{
                echo "
                <div class='form-group row'>
                    <hr>
                </div>
                <div class='form-group row'>
                <div class='col-sm-4'>Tipe Pembayaran</div>
                <div class='col-sm-1'>:</div>
                <div class='col-sm-7'>
                    $getDetail->tipe_pembayaran
                </div>
            </div>
            <div class='form-group row'>
                <div class='col-sm-4'>Jumlah Pembayaran</div>
                <div class='col-sm-1'>:</div>
                <div class='col-sm-7'>
                    $getDetail->jumlah_pembayaran
                </div>
            </div>
            <div class='form-group row'>
                <div class='col-sm-4'>Status Pembayaran</div>
                <div class='col-sm-1'>:</div>
                <div class='col-sm-7'>
                    $getDetail->status_pembayaran
                </div>
            </div>";
            }
            $iterasi++;
        }
    }

}
