<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    // protected $redirectTo='/login';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    public function tampillogin()
    {
        return view('auth/login');
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        $admin = Admin::where('email', $request->email)->first();

        if(!$admin)
        {
            return redirect('/login')->with('status','Email atau Password anda salah');
        }

        $credential=[
            'email'=>$request->email,
            'password'=>$request->password
        ];

        if (Auth::guard('admin')->attempt($credential, $request->member)) {
            // if successful, then redirect to their intended location
          return redirect()->intended('/admin/dashboard');
        }
        return redirect()->back()->withInput($request->only('email','remember'));
    }

    public function logout()
    {
        if (Auth::guard('admin')->check())
        {
            Auth::guard('admin')->logout();
        }
        return redirect('/login');
    }

    public function showLoginForm()
    {
        return view('auth/login');
    }
}
