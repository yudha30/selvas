<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use Illuminate\Support\Facades\DB;
use File;
use App\Katalog;

class TopiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=DB::table('katalog')->where('produk_id',10)->get();
        $produk=Produk::find(10);

        return view('admin.topi', compact('data','produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'keterangan' => 'required|max:255',
            'foto' => 'required|image|max:2048'
        ]);
        if($request->hasFile('foto')){
            $request->file('foto')->move('admin/katalog/topi/',date("dmYhis").$request->file('foto')->getClientOriginalName());
            $foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
        }

        DB::table('katalog')->insert([
            'produk_id' => $request->produk,
            'keterangan' => $request->keterangan,
            'foto' => $foto
        ]);
        return redirect('/admin/topi')->with('sukses','Data Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data=Produk::find(10);
        $katalog=Katalog::where('produk_id',10)->get();
        return view('produk.topi',compact('data','katalog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gambar = Katalog::where('kd_katalog',$id)->first();
        File::delete('admin/katalog/topi/'.$gambar->foto);
        DB::table('katalog')->where('kd_katalog', $id)->delete();
        return redirect('/admin/topi')->with('sukses','Data Berhasil Dihapus!');
    }
}
