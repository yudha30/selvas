<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use Illuminate\Support\Facades\DB;
use File;
use App\Bahan;
use App\Bahan_Produk;
use App\Katalog;
use App\Ukuran;

class ProdukController extends Controller
{


    // ============== view =======================
    public function kaos()
    {
        $data=Produk::where('id',1)->first();
        $katalog=Katalog::where('produk_id',1)->get();
        $produk=Produk::find(1);
        return view('produk.kaos',compact('data','katalog','produk'));
    }

    public function kemeja()
    {
        $data=Produk::find(7);
        $katalog=Katalog::where('produk_id',7)->get();
        return view('produk.kemeja',compact('data','katalog'));
    }

    // ============== akhir view =================




    public function menu_produk()
    {
        $data=Produk::all();
        foreach($data as $data)
        {
            echo '<a class="dropdown-item" href="/produk/'.$data->id.'">'.$data->nama_produk.'</a><div class="dropdown-divider"></div>';
        }
    }

    public function sortir_menu_produk($id)
    {
        $cek=0;
        $produk=Produk::all();
        $ukuran=Ukuran::all();
        $dataUkuran=Ukuran::where('produk_id',$id)->get();
        foreach($produk as $produk)
        {
            if($produk->id==$id){
                $data=Produk::find($id);
                $katalog=Katalog::where('produk_id',$id)->get();

            }
        }
        foreach($ukuran as $ukuran)
        {
            if($ukuran->produk_id==$id)
            {
                $cek=1;
            }
        }
        return view('produk.produk',compact('data','katalog','cek','dataUkuran'));
    }

    public function list_produk()
    {
        $data=Produk::all();
        foreach($data as $data)
        {
            echo '<li class="nav-item" ><a href="/admin/daftarkatalog/'.$data->id.'" class="nav-link"><i class="far fa-circle nav-icon"></i><p>'.$data->nama_produk.'</p></a></li>';
        }
    }





    // ============== halaman admin ==============

    public function daftarproduk()
    {
        $data=Produk::all();
        $bahan = Bahan::all();
        // dd($bahan[0]->nama_bahan);
        $bp = Bahan_Produk::all();
        return view('admin.produk',compact('data','bahan','bp')) ;
    }

    public function store(Request $request)
    {
        // $cekbox = count($request->select);
        // dd($request->select[1]);
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'foto' => 'required|image|max:2048',
        ]);

        $produk = Produk::where('nama_produk', $request->nama)->first();
        if($produk){
            // jika ada produk
            return redirect('/admin/tampilproduk')->with('gagal','Gagal Disimpan. Produk Sudah Ada!');
        }
        else{

            if($request->hasFile('foto')){
                $request->file('foto')->move('admin/produk/',date("dmYhis").$request->file('foto')->getClientOriginalName());
                $foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
            }

            DB::table('produk')->insert([
                'nama_produk' => $request->nama,
                'gambar_produk' => $foto,

            ]);

            $cekbox = count($request->select);
            $produk_id = DB::table('produk')->max('id');
            for ($i=0;$i<$cekbox;$i++)
            {
                DB::table('bahan_produk')->insert([
                    'bahan_id' => $request->select[$i],
                    'produk_id' => $produk_id,

                ]);
            }

            // $data=Produk::all();
            return redirect('/admin/tampilproduk')->with('sukses','Data Berhasil Ditambahkan!');
        }
    }

    public function update(Request $request, Produk $produk)
    {
        // dd($request->id);
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'foto' => 'image|max:2048'
            ]);
            // hapus file
        if($request->hasFile('foto')){
            $gambar = Produk::where('id',$request->id)->first();
            $masuk=Produk::where('id',$request->id)->first();
            File::delete('admin/produk/'.$gambar->gambar_produk);
            $request->file('foto')->move('admin/produk/',date("dmYhis").$request->file('foto')->getClientOriginalName());
            $masuk->foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
            DB::table('produk')->where('id', $request->id)->update([
                'gambar_produk' => $masuk->foto
            ]);

        }
        DB::table('produk')->where('id', $request->id)->update([
            'nama_produk' => $request->nama
        ]);

        $cekbox = count($request->select);
        DB::table('bahan_produk')->where('produk_id', $request->id)->delete();
        $produk_id = $request->id;
        for ($i=0;$i<$cekbox;$i++)
        {
            DB::table('bahan_produk')->insert([
                'bahan_id' => $request->select[$i],
                'produk_id' => $produk_id,

            ]);
        }

        return redirect('/admin/tampilproduk')->with('sukses','Data Berhasil Diubah!');
    }

    public function destroy($produk)
    {
        // hapus file
        $gambar = Produk::where('id',$produk)->first();
        File::delete('admin/produk/'.$gambar->gambar_produk);
        DB::table('produk')->where('id', $produk)->delete();
        DB::table('bahan_produk')->where('produk_id', $produk)->delete();
        return redirect('/admin/tampilproduk')->with('sukses','Data Berhasil Dihapus!');
    }


    public function cari($id)
    {
        $i=0;
        $j=0;
        $a=[];
        $f=[];
        $produk=Bahan_Produk::where('Produk_id',$id)->get();
        foreach($produk as $p)
        {
            $i++;
            $a[$i]=$p->bahan_id;
        }

        $bahan = Bahan::all();

        foreach($a as $a)
        {
            foreach($bahan as $b)
            {
                if($b->id == $a)
                {
                    $j++;
                    $f[$j]=$b->id;
                    $a = $b->nama_bahan;
                }
            }
            echo"<option value='$f[$j]'>$a</option>";
        }
    }

    // ============== akhir halaman admin ========
}
