<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NotifPesanan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Produk;
use App\Bahan;
use App\Pembayaran;
use App\Pesanan;
use App\Detail_Pemesanan;
use Auth;
use File;

class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // ================ view user ===================================

    public function tampilpesan()
    {
        $produk=Produk::all();
        $bahan=Bahan::all();
        return view('pesan.pesan', compact('produk','bahan'));
    }

    // ================ akhir view user =============================



    public function index(Request $request)
    {
        $cek=0;
        if($request->has('cari')){
            $cari = Pesanan::where('nama_pemesan', 'like', '%'.$request->cari.'%')
            ->orWhere('email', 'like', '%'.$request->cari.'%')
            ->orwhere('id','like','%'.$request->cari.'%')
            ->paginate(15);
            foreach($cari as $c){
                $cek++;
            }
            if($cek==0)
            {
                return redirect('/admin/pesanan')->with('gagal', 'Data Tidak Ditemukan');
            }
            else
            {
                return view('admin.pesanan',['data'=>$cari]);
            }
        }
        else{
            $data=Pesanan::orderBy('id', 'desc')->paginate(25);
            return view ('admin/pesanan',['data'=>$data]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->hasFile('desain'));
        $email=Auth::user()->email;
        $validatedData = $request->validate([
            'nama' => 'required',
            'telepon' => 'required|numeric',
            'alamat' => 'required',
            'ukuran' => 'required',
            'totalpesan' => 'required|numeric',
            'tglSelesai' => 'required',
            // 'desain' => 'file|mimetypes:psd,cdr'
        ]);
        // dd($request->all());

        DB::table('pesanan')->insert([
            'nama_pemesan' => $request->nama,
            'telepon' => $request->telepon,
            'alamat_kirim' => $request->alamat,
            'email' => $email,
            'tgl_pemesanan' => date('Y-m-d H:i:s'),
            'tgl_selesai' => $request->tglSelesai,
            'total_harga' => 0,
            'keterangan' => $request->keterangan,
            'status_pemesanan' => 'Baru',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        $desain="";
        if($request->hasFile('desain')){
            $request->file('desain')->move('admin/desain/',date("dmYhis").$request->file('desain')->getClientOriginalName());
            $desain=date("dmYhis").$request->file('desain')->getClientOriginalName();
        }
        $pemesanan_id=Pesanan::where('email',$email)->where('status_pemesanan','Baru')->first();
        DB::table('detail_pesanan')->insert([
            'pesanan_id'=>$pemesanan_id->id,
            'produk_id' => $request->produk,
            'bahan_id' => $request->bahan,
            'jumlah_ukuran' => $request->ukuran,
            'total_pesan' => $request->totalpesan,
            'sub_harga' => 0,
            'desain' => $desain
        ]);



        return redirect('/keranjang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'status' => 'required',
            // 'desain' => 'mimes:cdr,psd'
        ]);
        DB::table('pesanan')
        ->where('id', $id)
        ->update([
            'keterangan' => $request->keterangan,
            'status_pemesanan' => $request->status
        ]);
        $data=Pesanan::where('id',$id)->first();
        $tampung_pesanan=$data->status_pemesanan;
        $tampung_email=$data->email;
        $data_pesanan=array(
            'email_body'=> 'Status Pemesanan Anda = '.$tampung_pesanan,
        );

        \Mail::to($data->email)->send(new NotifPesanan($tampung_pesanan));
        return redirect('/admin/pesanan')->with('sukses','Data berhasil diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // dd($id);
        $hapus=DB::table('detail_pesanan')->where('pesanan_id',$id)->get();
        // dd($hapus);
        foreach($hapus as $h)
        {
            File::delete('admin/desain/'.$h->desain);
        }
        // dd('sukses hapus');
        DB::table('pesanan')->where('id',$id)->delete();
        DB::table('detail_pesanan')->where('pesanan_id',$id)->delete();
        return redirect('/admin/pesanan')->with('sukses','Data berhasil dihapus.');
    }

    // ================ keranjang ===============
    public function tampilkeranjang()
    {
        $cek=0;
        $email=Auth::user()->email;
        $pesanan=Pesanan::where('email',$email)->where('status_pemesanan','Baru')->get();
        // $detail=Detail_Pemesanan::where('pesanan_id',$pesanan->id)->get();
        $id_pesanan="";
        $relasi=DB::table('detail_pesanan')
        ->join('produk','produk.id','=','detail_pesanan.produk_id')
        ->join('bahan','bahan.id','=','detail_pesanan.bahan_id')
        ->join('pesanan','pesanan.id','=', 'detail_pesanan.pesanan_id')
        ->select('detail_pesanan.id','detail_pesanan.pesanan_id','pesanan.nama_pemesan','pesanan.email','pesanan.telepon','pesanan.alamat_kirim','pesanan.tgl_pemesanan','produk.nama_produk','bahan.nama_bahan','detail_pesanan.jumlah_ukuran','pesanan.tgl_selesai','pesanan.keterangan')
        ->where('pesanan.email',$email)->where('status_pemesanan','baru')
        ->get();
        // $cek=count($relasi);
        foreach($relasi as $r)
        {
            $cek=$cek+1;
            $id_pesanan=$r->pesanan_id;
        }
        if($cek == 0)
        {
            return redirect('/pesan')->with('gagal','Anda tidak memiliki pesanan.');
        }
        else{
            $daftar_produk=Produk::all();
            $daftar_bahan=Bahan::all();

            return view('keranjang.keranjang',compact('relasi','pesanan','daftar_produk','daftar_bahan','id_pesanan'));
        }
    }

    public function tambah_pemesanan(Request $request)
    {
        // dd($request->bahan);
        $validatedData = $request->validate([
            'ukuran' => 'required',
            'totalpesan' => 'required|numeric',
            // 'desain' => 'mimes:cdr,psd'
        ]);
        // dd($request->id);
        $desain="";
        if($request->hasFile('desain')){
            $request->file('desain')->move('admin/desain/',date("dmYhis").$request->file('desain')->getClientOriginalName());
            $desain=date("dmYhis").$request->file('desain')->getClientOriginalName();
        }
        DB::table('detail_pesanan')->insert([
            'pesanan_id'=>$request->id,
            'produk_id' => $request->produk,
            'bahan_id' => $request->bahan,
            'jumlah_ukuran' => $request->ukuran,
            'total_pesan' => $request->totalpesan,
            'sub_harga' => 0,
            'desain' => $desain
        ]);

        return redirect('/keranjang')->with('sukses','Data Berhasil Ditambahkan');
    }

    // ========================================== detail pesanan ===========================================
    public function lihat_detail($id)
    {
        $relasi=DB::table('detail_pesanan')
            ->join('produk','produk.id','=','detail_pesanan.produk_id')
            ->join('bahan','bahan.id','=','detail_pesanan.bahan_id')
            ->join('pesanan','pesanan.id','=', 'detail_pesanan.pesanan_id')
            ->select('detail_pesanan.id','detail_pesanan.pesanan_id','produk.nama_produk','bahan.nama_bahan','detail_pesanan.jumlah_ukuran','detail_pesanan.total_pesan','detail_pesanan.sub_harga','detail_pesanan.desain')
            ->where('pesanan.id',$id)
            ->get();
        return view('admin.detail_pesanan',compact('relasi'));

    }

    public function hapus_detail($d)
    {

        $id=Detail_Pemesanan::where('id',$d)->first();
        $pesanan_id=$id->pesanan_id;

        $hitung=Detail_Pemesanan::where('pesanan_id',$pesanan_id)->get();
        $jumlah=count($hitung);
        // dd($id->desain);
        // dd($jumlah);
        if($jumlah==1)
        {
            File::delete('admin/desain/'.$id->desain);
            Pesanan::destroy($pesanan_id);
            // Pembayaran::destroy($pesanan_id);
            $bayar=Pembayaran::where('pesanan_id',$pesanan_id)->first();
            // dd($bayar->bukti_pembayaran);
            if($bayar)
            {
                File::delete('admin/bukti_pembayaran/'.$bayar->bukti_pembayaran);
                DB::table('pembayaran')->where('pesanan_id',$pesanan_id)->delete();
            }
            Detail_Pemesanan::destroy($d);
            return redirect('/admin/pesanan')->with('sukses','Data Berhasil Dihapus!');
        }
        else{
            File::delete('admin/desain/'.$id->desain);
            Detail_Pemesanan::destroy($d);
            return redirect('/detail/pesanan/'.$pesanan_id)->with('sukses','Data Berhasil Dihapus!');
        }
    }

    public function edit_detail_pesanan(Request $request,$id)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'total_pesan' => 'required|numeric',
            'sub_harga' => 'required|numeric'
        ]);
        $detail=Detail_Pemesanan::where('id',$id)->first();
        // dd($detail->desain);
        $desain="";
        if($request->hasFile('desain')){
            File::delete('admin/desain/'.$detail->desain);
            $request->file('desain')->move('admin/desain/',date("dmYhis").$request->file('desain')->getClientOriginalName());
            $desain=date("dmYhis").$request->file('desain')->getClientOriginalName();
            DB::table('detail_pesanan')->where('id', $id)->update([
                    'desain' => $desain,
                ]);
        }
        // dd('ddd');
        DB::table('detail_pesanan')->where('id', $id)->update([
                'total_pesan' => $request->total_pesan,
                'sub_harga' => $request->sub_harga,
            ]);
        $pesanan=Detail_Pemesanan::where('pesanan_id',$detail->pesanan_id)->get();
        $hitung=0;
        $harga=0;
        foreach($pesanan as $pesanan)
        {
            $harga=$pesanan->sub_harga*$pesanan->total_pesan;
            $hitung=$hitung+$harga;
        }
        // dd($hitung);
        DB::table('pesanan')->where('id', $detail->pesanan_id)->update([
            'total_harga'=>$hitung,
        ]);

        return redirect('/detail/pesanan/'.$detail->pesanan_id)->with('sukses','Data Berhasil Diubah!');
    }



    // ================= hapus pesan dari pelanggan ============================

    public function hapus_pesan_pelanggan($id)
    {
        // dd($id);
        $id_detail=Detail_Pemesanan::where('id',$id)->first();
        $pesanan_id=$id_detail->pesanan_id;
        $hitung=Detail_Pemesanan::where('pesanan_id',$pesanan_id)->get();
        $jumlah=count($hitung);
        // dd($pesanan_id);
        File::delete('admin/desain/'.$id_detail->desain);
        // dd($id);
        if($jumlah==1)
        {
            Pesanan::destroy($pesanan_id);
            Detail_Pemesanan::destroy($id);
            return redirect('/keranjang')->with('sukses','Data Berhasil Dihapus!');
        }
        else{
            Detail_Pemesanan::destroy($id);
            return redirect('/keranjang')->with('sukses','Data Berhasil Dihapus!');
        }
    }

    // ================= akhir hapus pesan pelanggan ===========================

    // =================== download desain =====================================

    public function unduh($id)
    {
        // dd($id);
        $desain=Detail_Pemesanan::find($id);
        $file = public_path() . '/admin/desain/' . $desain->desain;//Mencari file dari storage htdocs
        return response()->download($file, $desain->desain); //Download file yang dicari berdasarkan nama file
    }

    // =================== akhir download desain ===============================

}
