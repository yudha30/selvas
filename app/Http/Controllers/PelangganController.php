<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\Pelanggan;

class PelangganController extends Controller
{

    //
    //controller login with google
    use AuthenticatesUsers;

    protected $guard = 'pelanggan';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('pelanggan');
    }

    // public function login()
    // {
    //     return view('auth/userlogin');
    // }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }


    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        //cek apakah user telah terdaftar
        $existingUser = Pelanggan::where('email', $user->email)->first();
        if($existingUser){
            // $pel = Pelanggan::all();
            Auth::guard('pelanggan')->login($existingUser, true);

            // dd(Auth::guard('pelanggan')->user()->name);
            // return view('pesan.pesan',['pelangganAktif'=>$existingUser]);
            return redirect('/pesan');
        } else {
            Pelanggan::create([
                'name' => $user->name,
                'email'=> $user->email,
                'provider'=> strtoupper($provider),
                'foto'=>$user->getAvatar(),
                ]);
            $existingUser = Pelanggan::where('email', $user->email)->first();
            if($existingUser){
                // $pel = Pelanggan::all();
                Auth::guard('pelanggan')->login($existingUser, true);

                // dd(Auth::guard('pelanggan')->user()->name);
                // return view('pesan.pesan',['pelangganAktif'=>$existingUser]);
                return redirect('/pesan');
            }

        }
    }

    public function logout()
    {
        if (Auth::guard('pelanggan')->check()) {
        Auth::guard('pelanggan')->logout();
        }
        return redirect('/');
    }
}
