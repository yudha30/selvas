<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pembayaran;

class Detail_Pembayaran extends Model
{
    protected $table='detail_pembayaran';
    protected $fillable = [
        'pembayaran_id',
        'tipe_pembayaran',
        'jumlah_pembayaran',
        'status_pembayaran',
        'snap_token'
    ];

    public function pembayaran()
    {
        return $this->hasOne(Pembayaran::class);
    }

    /**
     * Set status to Pending
     *
     * @return void
     */
    public function setPending()
    {
        $this->attributes['status_pembayaran'] = 'pending';
        self::save();
    }

    /**
     * Set status to Success
     *
     * @return void
     */
    public function setSuccess()
    {
        $this->attributes['status_pembayaran'] = 'success';
        self::save();
    }

    /**
     * Set status to Failed
     *
     * @return void
     */
    public function setFailed()
    {
        $this->attributes['status_pembayaran'] = 'failed';
        self::save();
    }

    /**
     * Set status to Expired
     *
     * @return void
     */
    public function setExpired()
    {
        $this->attributes['status_pembayaran'] = 'expired';
        self::save();
    }
}
