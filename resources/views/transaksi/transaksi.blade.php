@extends('template.main')

@section('link-css')
<link rel="stylesheet" href="{{asset('/assets/css/transaksi.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="/admin/plugin/fontawesome-free/css/all.min.css">
@endsection
@section('title','Transaksi')



@section('content')
<div class="container">
    <?php $inc=0; $tagihan=array();?>
    @foreach($DataTransaksi as $DataTransaksi)
    <?php $idCetak=0;?>
    <div class="main-transaksi">
        <h3 class="text-center transaksi">Transaksi</h3>
        <hr>
        @if(session('sukses'))
        <div class="alert alert-success col-4" role="alert">
            {{session('sukses')}}
        </div>
        @elseif(session('gagal'))
        <div class="alert alert-danger col-4" role="alert">
            {{session('gagal')}}
        </div>
        @endif
        <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; $id_pesan=0;?>
            @endif
        <div class="pemesan">

            <table>
                <tr>
                    <td width="100px">Nama</td>
                    <td width="10px">:</td>
                    <td><strong>{{$nama}}</strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><strong>{{$email}}</strong></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td><strong>{{$telepon}}</strong></td>
                </tr>
                <tr>
                <td valign="top">Alamat Kirim</td>
                    <td valign="top">:</td>
                    <td><strong>{{$alamat}}</strong></td>
                </tr>
            </table>
        </div>
        <div class="perusahaan">
            <table>
                <tr>
                    <td width="100px">Perusahaan</td>
                    <td width="10px">:</td>
                    <td>CV. Selvas</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>semestajogja@yahoo.com</td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td>(0274) 414305</td>
                </tr>
                <tr>
                    <td valign="top">Alamat</td>
                    <td valign="top">:</td>
                    <td>Salakan / Dk. Jotawang No. 306B RT 09, Bangunharjo, Sewon, Bantul, D. I. Yogyakarta</td>
                </tr>
            </table>
        </div>
        <div class="data-pesanan mt-5">
            <strong>Data Pesanan :</strong><br>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">No Pesan</th>
                        <th scope="col">Jenis Pesan</th>
                        <th scope="col">bahan</th>
                        <th scope="col">Jumlah Ukuran</th>
                        <th scope="col">Tanggal Selesai</th>
                        <th scope="col">Jumlah Pesan</th>
                        <th scope="col">Sub Harga</th>
                        <th scope="col">Total</th>
                    </tr>
                </thead>
                <tbody><?php $cek=0; $perbandingan=0; $total_harga=0; $temp=0; ?>
                    @foreach ($relasi as $p)

                    <?php $id_pesan=$p->pesanan_id; ?>
                    @if($cek!=0 && $DataTransaksi->id == $id_pesan)
                        <?php
                        if($temp>0){
                            $perbandingan=$p->pesanan_id;
                            if($cek==$perbandingan)
                            {
                            ?>
                            <tr>
                                <td>{{$p->pesanan_id}}</td>
                                <td>{{$p->nama_produk}}</td>
                                <td>{{$p->nama_bahan}}</td>
                                <td>{{$p->jumlah_ukuran}}</td>
                                <td>{{date("d/m/Y",strtotime($p->tgl_selesai))}}</td>
                                <td>{{$p->total_pesan}}</td>
                                <td>{{$p->sub_harga}}</td>
                                <td>
                                    <?php
                                    	$idCetak=$p->pesanan_id;
                                        $total=$p->sub_harga*$p->total_pesan;
                                        $total_harga=$total_harga+$total;
                                    ?>
                                    {{$total}}
                                </td>
                            </tr>

                            <?php }
                            $temp++;
                        }?>
                    @elseif($DataTransaksi->id == $id_pesan)
                    <?php
                        $cek=$id_pesan;
                        ?>
                        <tr>
                            <td>{{$p->pesanan_id}}</td>
                            <td>{{$p->nama_produk}}</td>
                            <td>{{$p->nama_bahan}}</td>
                            <td>{{$p->jumlah_ukuran}}</td>
                            <td>{{date("d/m/Y",strtotime($p->tgl_selesai))}}</td>
                            <td>{{$p->total_pesan}}</td>
                            <td>{{$p->sub_harga}}</td>
                            <td>
                                <?php
                                    $idCetak=$p->pesanan_id;
                                    $total=$p->sub_harga*$p->total_pesan;
                                    $total_harga=$total_harga+$total;
                                ?>
                                {{$total}}
                            </td>
                        </tr>
                        <?php
                        $temp++;
                    ?>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pembayaran d-flex justify-content-end">
            <table>
                <tr>
                    <td>Total Harga</td>
                    <td width="20px" align="center"> :</td>
                    <td>{{$total_harga}}</td>
                </tr>
                <tr>
                    <td>Total Pembayaran</td>
                    <td width="20px" align="center"> :</td>
                    <td> {{$total_bayar[$inc]}} </td>
                </tr>
                <tr>
                    <td>Sisa Tagihan</td>
                    <td width="20px" align="center"> :</td>
                    <?php $hitungTagihan=$total_harga-$total_bayar[$inc];
                     array_push($tagihan,$hitungTagihan)?>
                    <td> {{$tagihan[$inc]}} </td>
                </tr>
                <tr>
                    <td>Status Pemesanan</td>
                    <td width="20px" align="center"> :</td>
                    <td>{{$status_pesanan[$inc]}}</td>
                </tr>
                <tr>
                    <td>Status Pembayaran</td>
                    <td width="20px" align="center"> :</td>
                    <td>
                        @if($status_pesanan[$inc]=="Pending")
                        <strong>Menunggu Konfirmasi</strong>
                        @else
                        <strong>{{$status_bayar[$inc]}}</strong>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" class="lunas" value="lunas" name="lunas{{$inc}}">
                        <label for="lunas">Lunas</label><br>
                    </td>
                </tr>
            </table><br>
        </div>
        <div class="tombol-pembayaran d-flex justify-content-end mt-3">

                {{-- <button type="button" class="btn btn-primary mr-3"><i class="fas fa-upload mr-2"></i>bukti bayar</button> --}}
                @if ($status_bayar[$inc]=="Lunas" || $total_harga==0)
                {{-- <button type="button" class="btn btn-primary mr-2 " data-toggle="modal"
                data-target="#editModal{{$id}}"><i class="fas fa-upload"></i>&nbsp; Bukti Bayar</button> --}}
            <form action="/cetak/transaksi/{{$idCetak}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-secondary"><i class="fas fa-print"></i>&nbsp; Cetak</button>
                </form>
                @else
                <form class="form-horizontal" id="donation" onsubmit="return submitForm('{{$cek}}','{{$total_harga}}','lunas{{$inc}}');">
                    <button id="submit" class="btn btn-success mr-1"><i class="fas fa-plus-circle mr-2"></i>Bayar</button>
                </form>
                <form action="/cetak/transaksi/{{$idCetak}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-secondary"><i class="fas fa-print"></i>&nbsp; Cetak</button>
                </form>
                @endif
            <!-- Modal Edit-->
            <div class="modal fade" id="editModal{{$id}}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">

                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Upload Bukti Pembayaran</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <form action="/bukti/pembayaran/{{$id}}" method="post" enctype="multipart/form-data" id="formEdit">
                                @method('patch')
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Bukti Pembayaran</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control @error('bayar') is-invalid @enderror" id="bayar" name="bayar">
                                        @error('bayar')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        <img id="preview" src="{{url('/admin/icon_gallery/gallery.png')}}" alt="your image"
                                        width="300px">
                                    </div>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $inc++; ?>
    @endforeach
</div>
@section('js')
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
{{-- <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-94Ixzy4YgGLN_ryx"></script> --}}
<script>
    function submitForm($perbandingan,$total_harga,$lunas) {
        // Kirim request ajax
        let id = $perbandingan;
        let total_harga = $total_harga;
        let DownPayment = total_harga/2;
        let lunas = document.getElementsByName($lunas);
        for(var i = 0; i < lunas.length; i++){
            if(lunas[i].checked){
                DownPayment=total_harga;
            }
        }

        $.post("/bank/store",
        {
            _method: 'POST',
            _token: '{{ csrf_token() }}',
            amount: total_harga,
            dp: DownPayment,
            donation_type: 'Pembayaran',
            nama: '{{$nama}}',
            email: '{{$email}}',
            id_pesan: id,
            telepon:'{{$telepon}}',
        },
        function (data, status) {
            console.log(data,status);

            snap.pay(data.snap_token, {
                // Optional
                onSuccess: function (result) {
                    location.reload();
                },
                // Optional
                onPending: function (result) {
                    location.reload();
                },
                // Optional
                onError: function (result) {
                    location.reload();
                }
            });
        });
        // console.log(url);
        return false;

    }
    // console.log('ds');

</script>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }

  $("#bayar").change(function() {
      readURL(this);
  });
</script>
@endsection
@stop
