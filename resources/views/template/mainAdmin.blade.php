<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="_token" content="{{csrf_token()}}" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  {{-- <link rel="stylesheet" href="/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> --}}
  <!-- iCheck -->
  {{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css"> --}}
  <!-- JQVMap -->
  {{-- <link rel="stylesheet" href="/assets/plugins/jqvmap/jqvmap.min.css"> --}}
  <!-- Theme style -->
  {{-- <link rel="stylesheet" href="/assets/dist/css/adminlte.min.css"> --}}
  <!-- overlayScrollbars -->
  {{-- <link rel="stylesheet" href="/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> --}}
  <!-- Daterange picker -->
  {{-- <link rel="stylesheet" href="/assets/plugins/daterangepicker/daterangepicker.css"> --}}
  <!-- summernote -->
  {{-- <link rel="stylesheet" href="/assets/plugins/summernote/summernote-bs4.css"> --}}
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- Ekko Lightbox -->
  <link rel="stylesheet" href="/assets/plugins/ekko-lightbox/ekko-lightbox.css">


  <!-- Font Awesome -->
  <link rel="stylesheet" href="/admin/plugin/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  {{-- <link rel="stylesheet" href="/admin/css/adminlte.min.css"> --}}
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- bootstrap -->
  <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.min.css')}}">
  <!-- cssku -->
  <link rel="stylesheet" href="{{asset('/assets/css/styleadmin.css')}}">
  <link rel="stylesheet" href="{{asset('/assets/css/styleadmin_karyawan.css')}}">
  <link rel="stylesheet" href="{{asset('/assets/css/adminlte.min.css')}}">
  @yield('header')

  <title>Dashboard</title>
  <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('bd67db06993b549bbd9c', {
      cluster: 'ap1',
      forceTLS: true
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('form-notification', function(data) {
      let angka=parseInt(document.getElementById("notif").innerHTML);
      angka = angka + 1;
      // alert(JSON.stringify(data));
      alert('Ada Pesanan Baru. Silahkan Cek Pesanan!.');
      document.querySelector('.pemberitahuan').innerHTML = angka;
      document.querySelector('.pemberitahuan1').innerHTML = angka;
      document.querySelector('.pemberitahuan2').innerHTML = angka;
      // dd(angka);
    });

    var pembayaran = pusher.subscribe('bayar');
    pembayaran.bind('notification-bayar', function(data) {
      let angka=parseInt(document.getElementById("pembayaran").innerHTML);
      angka = angka + 1;
      // alert(JSON.stringify(data));
      alert('Ada Pembayaran Baru. Silahkan Cek Pembayaran!.');
      document.querySelector('.pembayaran').innerHTML = angka;
      document.querySelector('.pembayaran1').innerHTML = angka;
      document.querySelector('.pembayaran2').innerHTML = angka;
      // dd(angka);
    });
  </script>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">


  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link notif" data-toggle="dropdown" href="#">
          <i class="far fa-bell "></i>
          <span class="badge badge-warning navbar-badge pemberitahuan" id="notif">0</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"> <span class="pemberitahuan1">0</span> Notifikasi</span>
          <div class="dropdown-divider"></div>
          <a href="/admin/pesanan" class="dropdown-item">
            <i class="fas fa-shopping-cart mr-2"></i> <span class="pemberitahuan2">0</span> Pesanan Baru
          </a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link notif" data-toggle="dropdown" href="#">
            <i class="fas fa-wallet"></i>
          <span class="badge badge-warning navbar-badge pembayaran" id="pembayaran">0</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"> <span class="pembayaran1">0</span> Notifikasi</span>
          <div class="dropdown-divider"></div>
          <a href="/admin/pembayaran" class="dropdown-item">
            <i class="far fa-money-bill-alt mr-2"></i> <span class="pembayaran2">0</span> Pembayaran Baru
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/admin/dashboard')}}" class="brand-link">
      <img src="/admin/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Selvas</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <?php $foto=Auth::user()->foto;?>
          <img src="{{url('/admin/foto_admin/'.$foto)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="{{url('/admin/dashboard')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('/admin/karyawan')}}" class="nav-link">
            <i class="fas fa-user-cog"></i>
              <p>
                Data Admin
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('/admin/pelanggan')}}" class="nav-link">
            <i class="fas fa-users"></i>
              <p>
                Data Pelanggan
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('/admin/pesanan')}}" class="nav-link">
            <i class="fas fa-shopping-cart"></i>
              <p>
                Pesanan
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('/admin/pembayaran')}}" class="nav-link">
            <i class="far fa-money-bill-alt"></i>
              <p>
                Pembayaran
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('tampilproduk')}}" class="nav-link">
            <i class="fab fa-product-hunt"></i>
              <p>
                Produk
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('IndexUkuran')}}" class="nav-link">
                <i class="fas fa-ruler"></i>
              <p>
                Ukuran
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/bahan" class="nav-link">
            <i class="fas fa-tshirt"></i>
              <p>
                Bahan
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a class="nav-link">
            <i class="far fa-images"></i>
              <p>
                Katalog
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" id="list_produk">
              <li class="nav-item" >
                <a href="/admin/daftarkatalog" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kaos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/tampil_kemeja" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kemeja</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/jaket" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jaket</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/blazer" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blazer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/topi" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Topi</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{url('/admin/laporan')}}" class="nav-link">
            <i class="fas fa-clipboard-list"></i>
              <p>
                Laporan Pemesanan
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt"></i>
              <p>Keluar</p>
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


  @yield('content')


  <footer class="main-footer text-center">
    <div class="float-right d-none d-sm-block">
    </div>
    <span><strong>Copyright &copy; 2019 <a href="https://www.instagram.com/yudha.30/">mas_prass</a>.</strong> All rights
    reserved.</span>
  </footer>

  <!-- Control Sidebar -->
  <!-- <aside class="control-sidebar control-sidebar-dark"> -->
    <!-- Control sidebar content goes here -->
  <!-- </aside> -->
  <!-- /.control-sidebar -->
</div>
  <!-- ./wrapper -->
  <!-- jQuery -->
  <script src="/admin/plugin/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/admin/plugin/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/admin/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/admin/js/demo.js"></script>
  <script src="/assets/js/jquery-3.4.1.min.js"></script>
  <script src="/assets/js/popper.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  @yield('javascript_admin')

  <!-- ============================== resource admin lte ================================-->



  <!-- jQuery -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/dist/js/adminlte.js"></script>
<!-- Ekko Lightbox -->
<script src="/assets/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- Filterizr-->
<script src="/assets/plugins/filterizr/jquery.filterizr.min.js"></script>

  <!-- DataTables -->
  <script src="/assets/plugins/datatables/jquery.dataTables.js"></script>
  <script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });


  // ====== Preview Foto tambah data admin ========
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }

  $("#foto").change(function() {
      readURL(this);
  });


</script>

<!-- Page specific script -->
<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>

<!-- cek pilih bahan pada menu produk -->
<script>
  function check()
  {
      // let cek=0;


  }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        // document.getElementById('list_produk').innerHTML='da';
        $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            type:'GET',
            url:'/load/list_produk',
            success: function(response){
                $data=response;
                document.getElementById("list_produk").innerHTML=response;
                console.log($data);
            }
        });
    });
</script>
@yield('footer')
  <!-- ============================== akhir resource admin lte ================================-->

</body>
</html>
