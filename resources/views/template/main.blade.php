<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Viga&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/stylehome.css')}}">
    <link rel="stylesheet" href="{{asset('/css/aos.css')}}">
    @yield('link-css')
    <script src="/js/aos.js"></script>
    <title>@yield('title')</title>
  </head>
  <body>
      <!-- nav -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
            <a class="navbar-brand" href="{{url('/')}}"> <img src="{{url('/assets/img/logoselvas/selvas.png')}}" width="130px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto mx-3">
                    <a class="nav-item nav-link garis" href="{{url('/')}}"> <b>Home</b> </a>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle tulisan-produk" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <b>Produk</b>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="menu_produk">
                            <a class="dropdown-item" href="{{url('/kaos')}}">Kaos</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{url('/kemeja')}}">Kemeja</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{url('/jaket')}}">Jaket</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/topi">Topi</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/blazer">Blazer</a>

                        </div>
                    </li>
                    @guest
                    <a class="nav-item nav-link garis" href="{{url('auth/google')}}"> <b>Pesan</b> </a>

                    @else
                    <a class="nav-item nav-link garis" href="{{url('/pesan')}}"> <b>Pesan</b> </a>
                    @endif
                    <!-- Authentication Links -->

                    @guest
                            <li class="nav-item">
                                <a class="nav-link nav-link garis" href="{{route('login')}}"><b>Login</b></a>
                                <!-- <a class="nav-link nav-link garis" href="{{ url('auth/google') }}"><b>Login</b></a> -->
                            </li>
                        @else

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a href="/transaksi" class="dropdown-item">Transaksi</a>
                                    <a href="/keranjang" class="dropdown-item">Keranjang</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="/logout/user" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                </div>
            </div>
            </div>
        </nav>
    </header>
    <!-- akhir nav -->


    @yield('content')


    <div class="tampil">
        <div class="row text-right">
            <div class="col-12" id="tampil">
                <!-- tampilnya menggunakan javascript -->

            </div>
        </div>
        <div class="row wa">
            <div class="col-12" >
                <a href="https://web.whatsapp.com/send?phone=6281575774865&text=&source=&data=" target="_blank" id="sorot"><img src="{{url('/assets/img/wa.png')}}" class="img-fluid"></a>
            </div>
        </div>
    </div>



    <!-- footer -->
    <div class="footer mt-5 pb-4">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
                    Kantor
                    <div class="row mt-3">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1">
                            <img src="{{url('/assets/img/icon/footer/pointer.png')}}">
                        </div>
                        <div class="col-md-10 col-sm-10 col-10">
                        <p class="text-left">Salakan / Dk. Jotawang No. 306B RT 09, Bangunharjo, Sewon, Bantul, D. I. Yogyakarta.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
                    Kontak
                    <div class="row mt-3">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1">
                            <img src="{{url('/assets/img/icon/footer/email.png')}}">
                            <img src="{{url('/assets/img/icon/footer/call.png')}}"><br>
                        </div>
                        <div class="col-lg-10 col-md-5 col-sm-10  col-10">
                        <p class="text-left">semestajogja@yahoo.com<br>(0274) 414305</p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
                    Jam Kerja
                    <div class="row mt-3">
                        <div class="col-lg-1 col-md-1 col-sm-1 offset-1 col-1">
                            <img src="{{url('/assets/img/icon/footer/clock.png')}}">
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-10 col-7">
                            <p class="text-left">Senin-jumat 09.00 - 17.00 WIB <br>
                            Sabtu 09.00 - 15.00 WIB </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
                    Media Sosial
                    <div class="row mt-3">
                        <div class="col-lg-1 offset-2 col-md-1 col-sm-1 col-1">
                            <img src="{{url('/assets/img/icon/footer/facebook.png')}}">
                            <img src="{{url('/assets/img/icon/footer/ig.png')}}"width="16px" height="16px"><br>
                        </div>
                        <div class="col-lg-7 col-md-6 col-sm-7 col-8">
                        <p class="text-left">CV. Selvas  <br>@selvas_jogja</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <script src="/assets/js/jquery-3.4.1.min.js"></script>
    <script>
        AOS.init();
    </script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    @yield('js')
    <script>
        const sorot= document.querySelector('#sorot');
        const tampil= document.querySelector('#tampil');

        $(document).ready(function(){
            sorot.addEventListener('mouseover',function(){
                tampil.innerHTML="<span class='tulisan-wa'>Silahkan Bertanya Kepada Kami</span>";
            });
            sorot.addEventListener('mouseout',function(){
                tampil.innerHTML="";

            });
        });
    </script>

    <script>
        function periksa(){
            let produk=document.pesan.produk.value;
            let bahan=document.pesan.bahan.value;

            if(produk=="0"){
            alert("Field Produk Harus Diisi!");
            return false;
            }

            if(bahan=="0"){
            alert("Field Bahan Harus Diisi!");
            return false;
            }

        }
    </script>


    <!-- javascript seleksi bahan berdasarkan produk-->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#produk').change(function() { // Jika Select Box id produk dipilih
                let produk = $(this).val(); // Ciptakan variabel produk
                console.log(produk);
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'get', // Metode pengiriman data menggunakan POST
                    url: '/cari/produk/'+produk, // File yang akan memproses data
                    data: 'produk=' + produk, // Data yang akan dikirim ke file pemroses
                    success: function(response) { // Jika berhasil
                        $('#bahan').html(response); // Berikan hasil ke id bahan
                    }
                });
            });
            $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            $.ajax({
                type:'GET',
                url:'/load/menu_produk',
                success: function(response){
                    document.getElementById("menu_produk").innerHTML=response;
                    console.log(response);
                }
            });
        });
    </script>

    <!-- akhir javascript -->
  </body>
</html>
