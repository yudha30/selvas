<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/keranjang.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Viga&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/admin/plugin/fontawesome-free/css/all.min.css">
    <title>Keranjang</title>
</head>

<body>
    <?php $id=""; ?>
    <div class="container">
        <div class="header mt-5">
            <h2>
                <center> Pesanan</center>
            </h2>
        </div>
        <div class="identitas mt-5 mb-5 d-flex justify-conten-beetwen">
            <!-- identitas pemesan -->
            <div class="pemesan">
                <table>
                <?php $id=""; ?>
                @foreach($pesanan as $r)
                    <?php $id=$r->id ?>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><strong>{{$r->nama_pemesan}}</strong></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><strong>{{$r->email}}</strong></td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td>:</td>
                        <td><strong>{{$r->telepon}}</strong></td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat</td>
                        <td valign="top">:</td>
                        <td><strong>{{$r->alamat_kirim}}</strong></td>
                    </tr>
                    <tr>
                        <td valign="top">Tanggal Pesan</td>
                        <td valign="top">:</td>
                        <td><strong>{{date("d F Y H:i:s",strtotime($r->tgl_pemesanan))}}</strong></td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <!-- akhir identitas pemesan -->
            <div class="perusahaan">
                <table>
                    <tr>
                        <td>Perusahaan</td>
                        <td>:</td>
                        <td>CV. Selvas</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>semestajogja@yahoo.com</td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td>:</td>
                        <td>(0274) 414305</td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat</td>
                        <td valign="top">:</td>
                        <td>Salakan / Dk. Jotawang No. 306B RT 09,Bangunharjo, Sewon, Bantul, D. I. Yogyakarta</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row ml-1">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali !
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="daftar-pesanan">
            <table class="table">
                <thead>
                    <tr class="thead-dark">
                        <th scope="col">No</th>
                        <th scope="col">Kode Pemesanan</th>
                        <th scope="col">Jenis Pesanan</th>
                        <th scope="col">Bahan</th>
                        <th scope="col">jumlah</th>
                        <th scope="col">Tanggal Selesai</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($relasi as $relasi)
                    <?php //$id=$relasi->id ?>
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$relasi->pesanan_id}}</td>
                        <td>{{$relasi->nama_produk}}</td>
                        <td>{{$relasi->nama_bahan}}</td>
                        <td>{{$relasi->jumlah_ukuran}}</td>
                        <td>{{$relasi->tgl_selesai}}</td>
                        <td>{{$relasi->keterangan}}</td>

                        <td>
                            <form action="/hapus/detail/pesanan/{{$relasi->id}}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Yakin ingin menghapus admin tersebut ?')" title="Hapus"><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tombol d-flex justify-content-end mt-5">
            <!-- <button class="btn btn-primary mb-2 mr-2" type="button" data-toggle="modal" data-target="#exampleModal"
                id="open" data-id="{{$id}}"> <i class="fas fa-plus"></i>&nbsp; Pesanan</button> -->
            <button type="button" class="btn btn-primary btn-sm mr-2 mb-5" data-toggle="modal"
                data-target="#editModal{{$id}}"><i class="fas fa-plus"></i>&nbsp; Pesanan</button>

            <!-- Modal Edit-->
            <div class="modal fade" id="editModal{{$id}}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">

                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Edit Admin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/pesanan/tambah_pemesanan" method="post" onSubmit="return periksa()"
                                enctype="multipart/form-data" id="formEdit">
                                @method('patch')
                                @csrf
                                <div class="form-group">
                                    <input name="id" id="id" type="hidden" class="form-control" readonly
                                        value="{{$id}}">
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis Produk</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" name="produk" id="produk">
                                            <option value="0" selected>- Pilih Produk -</option>
                                            @foreach($daftar_produk as $p)
                                                <option value="{{$p->id}}">{{$p->nama_produk}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Bahan</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" name="bahan" id="bahan">
                                            <option value="0" selected>- Pilih Bahan -</option>
                                            @foreach($daftar_bahan as $b)
                                                <option value="{{$b->id}}">{{$b->nama_bahan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jumlah Ukuran</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" class="form-control @error('ukuran') is-invalid @enderror"
                                            id="ukuran" name="ukuran"
                                            placeholder="Masukkan Jumlah Ukuran ">{{old('ukuran')}}</textarea>
                                        @error('ukuran')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Pesan</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control @error('totalpesan') is-invalid @enderror"
                                            id="totalpesan" name="totalpesan" placeholder="Total Pesan"
                                            value="{{old('totalpesan')}}">
                                        @error('totalpesan')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Desain</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" id="desain" name="desain">
                                    </div>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- akhir Modal Edit -->

        <form action="/pesanan/selesai/{{$id_pesanan}}" method="post">
            @csrf
                <button class="btn btn-success mb-5">Selesai</button>
            </form>
        </div>
    </div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/jquery-3.4.1.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <script>
        function periksa() {
            let produk = $("#produk").val();
            let bahan = $("#bahan").val();

            if (produk == "0") {
                alert("Field Produk Harus Diisi!");
                return false;
            }

            if (bahan == "0") {
                alert("Field Bahan Harus Diisi!");
                return false;
            }
        }

    </script>

    <!-- javascript seleksi bahan berdasarkan produk-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#produk').change(function () { // Jika Select Box id produk dipilih
                let produk = $(this).val(); // Ciptakan variabel produk
                console.log(produk);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'get', // Metode pengiriman data menggunakan POST
                    url: '/cari/produk/' + produk, // File yang akan memproses data
                    data: 'produk=' + produk, // Data yang akan dikirim ke file pemroses
                    success: function (response) { // Jika berhasil
                        $('#bahan').html(response); // Berikan hasil ke id kota
                    }
                });
            });
        });

    </script>
</body>

</html>
