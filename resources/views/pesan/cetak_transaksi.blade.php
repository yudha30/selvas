<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Transaksi</title>
    <style>

        .main-transaksi
        {
            background-color: white;
            box-shadow: 1px 1px 6px 2px rgba(0, 0, 0, 0.274);
            border: 1px;
            margin-top: 10%;
            padding-left: 5%;
            padding-right: 5%;
            padding-bottom: 5%;
            border-radius: 10px;
        }

        .transaksi{
            padding-top: 4%;
            /* padding-bottom: 4%; */
        }

        .pemesan
        {
            width: 48%;
            display: flex;
            float: left;
            font-size: 10px;
        }

        .perusahaan{
            margin-left: 50%;
            display: flex;
            float: right;
            font-size:10px;
        }

        .data-pesanan{
            margin-top: 3%;
        }

        table.data{
            border-collapse: collapse;
            font-size: 12px;
        }

        .resume
        {
            margin-top:2%;
            margin-left:73%;
            font-size: 11px;
        }

    </style>
</head>
<body>
    <center><h3>Transaksi</h3></center>
    <hr>
    <br>
    <div class="pemesan">
        <table>
            <tr>
                <td width="50px">Nama</td>
                <td width="10px">:</td>
                <td><strong>{{$nama}}</strong></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><strong>{{$email}}</strong></td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>:</td>
                <td><strong>{{$telepon}}</strong></td>
            </tr>
            <tr>
            <td valign="top">Alamat Kirim</td>
                <td valign="top">:</td>
                <td><strong>{{$alamat}}</strong></td>
            </tr>
        </table>
    </div>
    <div class="perusahaan">
        <table>
            <tr>
                <td width="50px">Perusahaan</td>
                <td width="10px">:</td>
                <td>CV. Selvas</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td>semestajogja@yahoo.com</td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>:</td>
                <td>(0274) 414305</td>
            </tr>
            <tr>
                <td valign="top">Alamat</td>
                <td valign="top">:</td>
                <td>Salakan / Dk. Jotawang No. 306B RT 09, Bangunharjo, Sewon, Bantul, D. I. Yogyakarta</td>
            </tr>
        </table>
    </div>
    <div class="data-pesanan">
        Data Pesanan :<br>
        <table border="1" class="data">
            <thead>
                <tr>
                    <th scope="col">No Pesan</th>
                    <th scope="col" width="70">Jenis Pesan</th>
                    <th scope="col" width="70">bahan</th>
                    <th scope="col" width="100">Jumlah Ukuran</th>
                    <th scope="col" width="60">Tanggal Selesai</th>
                    <th scope="col" width="70">Jumlah Pesan</th>
                    <th scope="col" width="50">Sub Harga</th>
                    <th scope="col" width="50">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $total_harga=0;
                ?>
                @foreach ($relasi as $p)
                <tr>
                    <?php $id_pesan=$p->pesanan_id; ?>
                    <td>{{$p->pesanan_id}}</td>
                    <td>{{$p->nama_produk}}</td>
                    <td>{{$p->nama_bahan}}</td>
                    <td>{{$p->jumlah_ukuran}}</td>
                    <td>{{date("d/m/Y",strtotime($p->tgl_selesai))}}</td>
                    <td>{{$p->total_pesan}}</td>
                    <td>{{$p->sub_harga}}</td>
                    <td>
                        <?php
                            $total=$p->sub_harga*$p->total_pesan;
                            $total_harga=$total_harga+$total;
                        ?>
                        {{$total}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="resume">
        <table>
            <tr>
                <td width="90px">Total Harga</td>
                <td width="20px" align="center"> :</td>
                <td> {{$total_harga}} </td>
            </tr>
            <tr>
                <td>Total Pembayaran</td>
                <td width="20px" align="center"> :</td>
                <td> <strong>{{$total_bayar}}</strong> </td>
            </tr>
            <tr>
                <td>Status Pemesanan</td>
                <td width="20px" align="center"> :</td>
                <td> {{$pesan->status_pemesanan}} </td>
            </tr>
            <tr>
                <td>Status Pembayaran</td>
                <td width="20px" align="center"> :</td>
                <td> <strong>{{$status_bayar}}</strong> </td>
            </tr>
        </table><br>
    </div>

</body>
</html>
