@extends('template.main')

@section('title','Pesan')

@section('content')
    <!-- ketentuan pesan -->
    <div class="container">
        <div class="row pesan">
            <div class="col-lg-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Ketentuan Pesanan</h4>
                    
                   
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1 text-right">
                           1.
                        </div>
                        <div class="col-lg-8 col-md-11 col-sm-11 col-11 text-justify">
                            Biaya ditentukan berdasarkan tingkat kerumitan desain pesanan dan bahan kain yang digunakan.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1 text-right">
                           2.
                        </div>
                        <div class="col-lg-8 col-md-11 col-sm-11 col-11 text-justify">
                            Minimal pembayaran DP 50% atau bisa dikonfirmasi melalui whatsapp.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1 text-right">
                           3.
                        </div>
                        <div class="col-lg-8 col-md-11 col-sm-11 col-11 text-justify">
                            Minimal pesanan 20pcs. Apabila dibawahnya, maka dikenakan harga khusus.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1 text-right">
                           4.
                        </div>
                        <div class="col-lg-8 col-md-11 col-sm-11 col-11 text-justify">
                            Warna bahan desesuaikan dengan katalog milik CV. Selvas.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1 text-right">
                           6.
                        </div>
                        <div class="col-lg-8 col-md-11 col-sm-11 col-11 text-justify">
                        Tidak menerima komplain ketika form approval desain sudah disetujui dan kami terima berikut DP. Apabila ada revisi desain, tambahan ataupun pengurangan begitu juga kuantitas setelahnya, dianggap pemesanan/pengajuan baru. Produksi tetap berlanjut..
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1 text-right">
                           7.
                        </div>
                        <div class="col-lg-8 col-md-11 col-sm-11 col-11 text-justify">
                            Biaya pengiriman ditangggung pelanggan.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- akhir ketentuan pesan -->
    <hr class="mt-4">


    <!-- Daftar bank -->
    <div class="container">
        <div class="row mt-5 visi">
            <div class="col-lg-6 col-md-12">
                <h4> <strong>Daftar Nomor Rekening CV. Selvas</strong> </h4> 
            </div>
        </div>
        <div class="row visi mt-2">
            <div class="col-lg-1 col-md-2 col-sm-2 col-2">
                <img src="{{url('/assets/img/bank/bni.png')}}"class="img-fluid">
            </div>
            <div class="col-8 ">Nomor Rekening : 003 0150 948</div>
        </div>
        <div class="row visi mt-2">
            <div class="col-lg-1 col-md-2 col-sm-2 col-2">
                <img src="{{url('/assets/img/bank/mandiri.png')}}"class="img-fluid">
            </div>
            <div class="col-8 ">Nomor Rekening : 137 000 3046 733</div>
        </div>
        <div class="row visi mt-2">
            <div class="col-lg-1 col-md-2 col-sm-2 col-2">
                <img src="{{url('/assets/img/bank/bca.png')}}"class="img-fluid">
            </div>
            <div class="col-8 ">Nomor Rekening : 445 0767 552</div>
        </div>
        <div class="row visi mt-2">
            <div class="col-lg-1 col-md-2 col-sm-2 col-2">
                <img src="{{url('/assets/img/bank/bri.png')}}"class="img-fluid">
            </div>
            <div class="col-8 ">Nomor Rekening : 0245 01 029 784 504</div>
        </div>
        <div class="row visi mt-2">
            <div class="col-lg-1 col-md-2 col-sm-2 col-2">
                <img src="{{url('/assets/img/bank/bpd-diy.png')}}"class="img-fluid">
            </div>
            <div class="col-8 ">Nomor Rekening : 006 211 025569</div>
        </div>
        <div class="row visi mt-2">
            <div class="col-12 col-md-7 col-sm-12 offset-1">
                <strong>a.n. Iskandarsyah Putra</strong>
            </div>
            
        </div>
    </div>
    <!-- Daftar bank -->
<hr class="mt-5">






    <!-- form pemesanan -->
    <div class="container visi">
        <div class=" mt-5">
            <div class="row">
                <div class="col-8 offset-2 text-center mb-2">
                    <h4> <strong>Formulir Tambah Pemesanan</strong> </h4>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-10 formulir">
                    <form action="/pesanan/tambah_pemesanan" method="post" onSubmit="return periksa()" name="pesan">
                    @csrf
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jenis Produk</label>
                            <div class="col-sm-8">
                                <select class="custom-select" name="produk" id="produk">
                                    <option value="0" selected>- Pilih Produk -</option>
                                    @foreach($produk as $p)
                                        <option value="{{$p->id}}">{{$p->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Bahan</label>
                            <div class="col-sm-8">
                                <select class="custom-select" name="bahan" id="bahan">
                                    <option value="0" selected>- Pilih Bahan -</option>
                                    @foreach($bahan as $b)
                                        <option value="{{$b->id}}">{{$b->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Ukuran</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control @error('ukuran') is-invalid @enderror" id="ukuran" name="ukuran" placeholder="Masukkan Jumlah Ukuran ">{{old('ukuran')}}</textarea>
                                @error('ukuran')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Total Pesan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('totalpesan') is-invalid @enderror" id="totalpesan" name="totalpesan" placeholder="Total Pesan" value="{{old('totalpesan')}}">
                                @error('totalpesan')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Desain</label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" id="desain" name="desain">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Tanggal Selesai</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control @error('tglSelesai') is-invalid @enderror" id="tglSelesai" name="tglSelesai" placeholder="Tanggal Selesai" value="{{old('tglSelesai')}}">
                                @error('tglSelesai')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Keterangan</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">{{old('keterangan')}}</textarea>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary">Pesan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- akkhir form pemesanan -->

@endsection