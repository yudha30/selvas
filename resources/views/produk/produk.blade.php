@extends('template.main')

@section('title','Produk '.$data->nama_produk)

@section('content')

<div class="container panel-kaos">
    <div class="row mt-5 visi">
        <div class="col kaos">
            <h2>{{$data->nama_produk}}</h2>
        </div>

        <!-- breadcrumb -->
        <div class="col-8 kaos ">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white">
                    <li class="breadcrumb-item ml-auto"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
        </div>
        <!-- akhir breadcrumb -->
    </div>

    <!-- ukuran kaos -->

    <div class="row mt-5 visi">
        <div class="col-lg-3 col-md-4 col-10 ukuran-kaos">
            {{-- <img src="{{url('/assets/img/produk/kaos/kaos.png')}}" class="img-fluid"> --}}
            <img src="{{url('/admin/produk/'.$data->gambar_produk)}}" class="img-fluid">
        </div>
        @if($cek==0)
        <div class="col-lg-8 col-md-7 offset-1 mt-0">
            <div class="row mt-5">
                <div class="col-lg-4">
                    <h4 class="visi">Daftar Bahan</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col" width="200px" class="text-center">Bahan</th>
                            <th scope="col" class="text-center">Deskripsi</th>
                            </tr>
                        </thead>
                        @foreach ($data->bahan as $p)
                        <tbody>
                            <tr>
                                <th scope="row">{{$p->nama_bahan}}</th>
                            <td class="text-justify">{{$p->keterangan}}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            </div>
        @else
        <div class="col-lg-8 col-md-7 offset-1 mt-5">
        <h4 class="visi">Daftar Ukuran {{$data->nama_produk}}</h4>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">Size</th>
                <th scope="col" class="text-center">S</th>
                <th scope="col" class="text-center">M</th>
                <th scope="col" class="text-center">L</th>
                <th scope="col" class="text-center">XL</th>
                <th scope="col" class="text-center">XXL</th>
                <th scope="col" class="text-center">XXXL</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dataUkuran as $item)
                <tr>
                    <th scope="row">{{$item->dimensi}}</th>
                    <td class="text-center">{{$item->s}} cm</td>
                    <td class="text-center">{{$item->m}} cm</td>
                    <td class="text-center">{{$item->l}} cm</td>
                    <td class="text-center">{{$item->xl}} cm</td>
                    <td class="text-center">{{$item->xxl}} cm</td>
                    <td class="text-center">{{$item->xxxl}} cm</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>

    </div>
    <!-- akhir ukuran Kaos -->
    <div class="row mt-5">
        <div class="col-lg-4">
            <h4 class="visi">Daftar Bahan</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th scope="col" width="200px" class="text-center">Bahan</th>
                    <th scope="col" class="text-center">Deskripsi</th>
                    </tr>
                </thead>
                @foreach ($data->bahan as $p)
                <tbody>
                    <tr>
                        <th scope="row">{{$p->nama_bahan}}</th>
                    <td class="text-justify">{{$p->keterangan}}</td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
        @endif
    </div>
    <!-- akhir ukuran kaos -->

    <!-- Katalog -->
    <div class="row mt-5 mb-3">
        <div class="col-lg-4">
        <h4 class="visi">Katalog {{$data->nama_produk}}</h4>
        </div>
    </div>
    <div class="row ">
        <div class="col-lg-12">
            <div class="row">
                @foreach ($katalog as $k)
                <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                    <div class="card mb-3">
                        <div class="card-body">
                            <img src="{{url('/admin/katalog/'.$data->nama_produk.'/'.$k->foto)}}" class="img-fluid" style="height:200px !important;">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Akhir Katalog -->

</div>


@endsection
