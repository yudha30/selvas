@extends('template.main')

@section('title','Produk Kemeja')

@section('content')

<div class="container panel-kaos">
    <div class="row mt-5 visi">
        <div class="col kaos">
            <h2>Kemeja</h2>
        </div>

        <!-- breadcrumb -->
        <div class="col-8 kaos ">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white">
                    <li class="breadcrumb-item ml-auto"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
        </div>
        <!-- akhir breadcrumb -->
    </div>

    <!-- ukuran kaos -->
    <div class="row mt-5 visi">
        <div class="col-lg-3 col-md-4 col-10 ukuran-kaos">
            <img src="{{url('/admin/produk/'.$data->gambar_produk)}}" class="img-fluid">
        </div>
        <div class="col-lg-8 col-md-7 offset-1 mt-5">
        <h4 class="visi">Daftar Ukuran Kemeja</h4>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">Size</th>
                <th scope="col" class="text-center">S</th>
                <th scope="col" class="text-center">M</th>
                <th scope="col" class="text-center">L</th>
                <th scope="col" class="text-center">XL</th>
                <th scope="col" class="text-center">XXL</th>
                <th scope="col" class="text-center">XXXL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Panjang</th>
                    <td class="text-center">65 cm</td>
                    <td class="text-center">67 cm</td>
                    <td class="text-center">71 cm</td>
                    <td class="text-center">73 cm</td>
                    <td class="text-center">78 cm</td>
                    <td class="text-center">80 cm</td>
                </tr>
                <tr>
                    <th scope="row">Lebar</th>
                    <td class="text-center">46 cm</td>
                    <td class="text-center">48 cm</td>
                    <td class="text-center">52 cm</td>
                    <td class="text-center">55 cm</td>
                    <td class="text-center">60 cm</td>
                    <td class="text-center">63 cm</td>
                </tr>

            </tbody>
        </table>
        </div>
    </div>
    <!-- akhir ukuran Kaos -->
    <div class="row mt-5">
        <div class="col-lg-4">
            <h4 class="visi">Daftar Bahan</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th scope="col" width="200px" class="text-center">Bahan</th>
                    <th scope="col" class="text-center">Deskripsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data->bahan as $d)
                    <tr>
                        <th scope="row">{{$d->nama_bahan}}</th>
                    <td class="text-justify">{{$d->keterangan}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- akhir ukuran kaos -->

    <!-- Katalog -->
    <div class="row mt-5 mb-3">
        <div class="col-lg-4">
            <h4 class="visi">Katalog Kemeja</h4>
        </div>
    </div>
    <div class="row ">
        <div class="col-lg-12">
            <div class="row">
                @foreach ($katalog as $k)
                <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                    <div class="card mb-3" style="height:210px !important;">
                        <div class="card-body">
                            <img src="{{url('/admin/katalog/kemeja/'.$k->foto)}}" class="img-fluid" style="height:160px !important; width:320px !important;">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Akhir Katalog -->

</div>


@endsection
