@extends ('template.mainAdmin')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Data Ukuran Produk</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    {{-- Main Content --}}
    <div class="container kontener">
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="row">
            <button class="btn btn-primary mb-2" type="button" data-toggle="modal" data-target="#exampleModal"
                id="open"> <i class="fas fa-plus"></i>&nbsp; Ukuran</button>

            {{-- <form class="form-inline mb-2 ml-auto" action="/admin/karyawan" method="GET">
                <input name="cari" id="cari" class="form-control mr-sm-2 " type="search" placeholder="Cari Admin"
                    aria-label="Search" value="{{ old('cari')}}">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i
                        class="fas fa-search"></i></button>
            </form> --}}
            <!-- Modal Tambah-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Ukuran</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/admin/ukuran" method="post" id="formTambah">
                                @csrf

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Produk</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" name="produk" id="produk" required="">
                                            <option value="" selected>- Pilih Produk -</option>
                                            @foreach($produk as $p)
                                                <option value="{{$p->id}}">{{$p->nama_produk}}</option>
                                            @endforeach
                                        </select>
                                        @error('produk')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Dimensi</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" name="dimensi" id="dimensi" required="">
                                            <option value="" selected>- Pilih Dimensi -</option>
                                                <option value="Panjang">Panjang</option>
                                                <option value="Lebar">Lebar</option>
                                                <option value="Tinggi">Tinggi</option>
                                        </select>
                                        @error('dimensi')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ukuran S</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control @error('S') is-invalid @enderror" id="S" name="S" placeholder="Ukuran S" value="{{old('S')}}" required>
                                        @error('S')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ukuran M</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control @error('M') is-invalid @enderror" id="M" name="M" placeholder="Ukuran M" value="{{old('M')}}" required>
                                        @error('M')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ukuran L</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control @error('L') is-invalid @enderror" id="L" name="L" placeholder="Ukuran L" value="{{old('L')}}" required>
                                        @error('L')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ukuran XL</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control @error('XL') is-invalid @enderror" id="XL" name="XL" placeholder="Ukuran XL" value="{{old('XL')}}" required>
                                        @error('XL')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ukuran XXL</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control @error('XXL') is-invalid @enderror" id="XXL" name="XXL" placeholder="Ukuran XXL" value="{{old('XXL')}}" required>
                                        @error('XXL')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ukuran XXXL</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control @error('XXXL') is-invalid @enderror" id="XXXL" name="XXXL" placeholder="Ukuran XXXL" value="{{old('XXXL')}}" required>
                                        @error('XXXL')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- akhir Modal Tambah -->
            <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="table-primary">
                        {{-- <th scope="col">No</th> --}}
                        <th scope="col">Nama Produk</th>
                        <th scope="col">Dimensi</th>
                        <th scope="col">S</th>
                        <th scope="col">M</th>
                        <th scope="col">L</th>
                        <th scope="col">XL</th>
                        <th scope="col">XXL</th>
                        <th scope="col">XXXL</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0;?>
                    @foreach ($data as $a)
                    <?php $no++ ; ?>
                    <tr>
                        {{-- <th scope="row">{{$no}}</th> --}}
                        <td>{{$a->produk->nama_produk}}</td>
                        <td>{{$a->dimensi}}</td>
                        <td>{{$a->s}} cm</td>
                        <td>{{$a->m}} cm</td>
                        <td>{{$a->l}} cm</td>
                        <td>{{$a->xl}} cm</td>
                        <td>{{$a->xxl}} cm</td>
                        <td>{{$a->xxxl}} cm</td>
                        <td>

                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                data-target="#editModal{{$a->id}}" data-id="{{$a->id}}" data-name="{{$a->produk->nama_produk}}"
                                data-dimensi="{{ $a->dimensi }}" data-s="{{$a->s}}" data-m="{{$a->m}}" data-l="{{$a->l}}" data-xl="{{$a->xl}}" data-xxl="{{$a->xxl}}" data-xxxl="{{$a->xxxl}}"><i
                                    class="far fa-edit"></i></button>

                            <!-- Modal Edit-->
                            <div class="modal fade" id="editModal{{$a->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">

                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Form Edit Ukuran</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <form action="/admin/ukuran/{{ $a->id }}" method="post" enctype="multipart/form-data" id="formEdit">
                                                @method('patch')
                                                @csrf
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Produk</label>
                                                    <div class="col-sm-8">
                                                        <select class="custom-select" name="produk" id="produk" required="">
                                                            @foreach($produk as $p)
                                                                <?php if($a->produk->nama_produk==$p->nama_produk){ ?>
                                                                <option value="{{$p->id}}" selected>{{$p->nama_produk}}</option>
                                                                <?php }else { ?>
                                                                    <option value="{{$p->id}}">{{$p->nama_produk}}</option>
                                                                <?php }?>
                                                            @endforeach
                                                        </select>
                                                        @error('produk')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Dimensi</label>
                                                    <div class="col-sm-8">
                                                        <select class="custom-select" name="dimensi" id="dimensi" required="">
                                                                <option value="Panjang" @if($a->dimensi=="Panjang") selected @endif>Panjang</option>
                                                                <option value="Lebar" @if($a->dimensi=="Lebar") selected @endif>Lebar</option>
                                                                <option value="Tinggi" @if($a->dimensi=="Tinggi") selected @endif>Tinggi</option>
                                                        </select>
                                                        @error('dimensi')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Ukuran S</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control @error('S') is-invalid @enderror" id="S" name="S" placeholder="Ukuran S" value={{$a->s}}  required>
                                                        @error('S')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Ukuran M</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control @error('M') is-invalid @enderror" id="M" name="M" placeholder="Ukuran M" value={{$a->m}}  required>
                                                        @error('M')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Ukuran L</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control @error('L') is-invalid @enderror" id="L" name="L" placeholder="Ukuran L" value={{$a->l}}  required>
                                                        @error('L')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Ukuran XL</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control @error('XL') is-invalid @enderror" id="XL" name="XL" placeholder="Ukuran XL" value={{$a->xl}}  required>
                                                        @error('XL')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Ukuran XXL</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control @error('XXL') is-invalid @enderror" id="XXL" name="XXL" placeholder="Ukuran XXL" value={{$a->xxl}}  required>
                                                        @error('XXL')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Ukuran XXXL</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control @error('XXXL') is-invalid @enderror" id="XXXL" name="XXXL" placeholder="Ukuran XXXL" value={{$a->xxxl}}  required>
                                                        @error('XXXL')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- akhir Modal Edit -->


                            <form action="/admin/ukuran/{{ $a->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Yakin ingin menghapus ukuran tersebut ?')"><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        <div class="links">
            {{ $data->links() }}
        </div>
    </div>
    {{-- Main Content --}}
</div>

@endsection
