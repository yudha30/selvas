@extends ('template.mainAdmin')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Pesanan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->


    <div class="container kontener">
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="row">
            <!-- <button class="btn btn-primary mb-2" type="button" data-toggle="modal" data-target="#exampleModal"
                id="open"> <i class="fas fa-plus"></i>&nbsp; Admin</button> -->

            {{-- <form class="form-inline mb-2 ml-auto" action="/admin/karyawan" method="GET">
                <input name="cari" id="cari" class="form-control mr-sm-2 " type="search" placeholder="Search"
                    aria-label="Search" value="{{ old('cari')}}">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i
                        class="fas fa-search"></i></button>
            </form> --}}
            <!-- Modal Tambah-->
            <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Admin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/admin/store" method="post" enctype="multipart/form-data" id="formTambah">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Lengkap</label>
                                    <input name="name" id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{old('name')}}">
                                    @error('name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label>
                                    <input name="email" id="email" type="text"
                                        class="form-control @error('email') is-invalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail"
                                        value="{{old('email')}}">
                                    @error('email')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input name="password" id="password" type="password"
                                        class="form-control @error('password') isinvalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password">
                                    @error('password')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat</label>
                                    <textarea name="alamat" id="alamat"
                                        class="form-control @error('alamat') is-invalid @enderror"
                                        id="exampleFormControlTextarea1" rows="3">{{old('alamat')}}</textarea>
                                    @error('alamat')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Telepon</label>
                                    <input name="telepon" id="telepon" type="text"
                                        class="form-control @error('telepon') is-invalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Telepon"
                                        value="{{old('telepon')}}">
                                    @error('telepon')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">foto</label>
                                    <input name="foto" id="foto" type="file"
                                        class="form-control @error('foto') is-invalid @enderror"
                                        value="{{old('foto')}}"><br>
                                    <img id="preview" src="{{url('/admin/icon_gallery/gallery.png')}}" alt="your image"
                                        width="300px">
                                    @error('foto')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir Modal Tambah -->

            <table class="table table-sm">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">Kode Pesanan</th>
                        <th scope="col">Produk</th>
                        <th scope="col">Bahan</th>
                        <th scope="col">Jumlah Ukuran</th>
                        <th scope="col">Pesan</th>
                        <th scope="col">Sub Harga</th>
                        <th scope="col">Desain</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($relasi as $relasi)
                    <tr>
                        <td>{{$relasi->pesanan_id}}</td>
                        <td>{{$relasi->nama_produk}}</td>
                        <td>{{$relasi->nama_bahan}}</td>
                        <td>{{$relasi->jumlah_ukuran}}</td>
                        <td>{{$relasi->total_pesan}}</td>
                        <td>{{$relasi->sub_harga}}</td>
                        <td>
                                <a href="/download/desain/{{$relasi->id}}">{{$relasi->desain}}</a>
                            </td>
                        <td>

                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                data-target="#editModal{{$relasi->id}}" ><i
                                    class="far fa-edit"></i></button>
                            <!-- Modal Edit-->
                            <div class="modal fade" id="editModal{{$relasi->id}}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">

                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Form Edit Detail Pesanan</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                            <form action="/edit/detail/pesanan/{{$relasi->id}}" method="post" enctype="multipart/form-data"
                                                    id="formEdit">
                                                    @method('patch')
                                                    @csrf
                                                    <div class="form-group">
                                                        <!-- <label for="exampleInputEmail1">ID</label> -->
                                                        <input name="id" id="id" type="hidden" class="form-control" readonly
                                                            value="{{$relasi->id}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Total Pesan</label>
                                                        <input name="total_pesan" id="total_pesan" type="text"
                                                            class="form-control @error('total_pesan') is-invalid @enderror"
                                                            placeholder="Total Pesan" value="{{$relasi->total_pesan}}"
                                                            value="{{ old($relasi->total_pesan)}}">
                                                        @error('total_pesan')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Sub Harga</label>
                                                        <input name="sub_harga" id="sub_harga" type="text"
                                                            class="form-control @error('sub_harga') is-invalid @enderror"
                                                            id="exampleInputEmail1" aria-describedby="emailHelp"
                                                            placeholder="Sub Harga" value="{{$relasi->sub_harga}}">
                                                        @error('sub_harga')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Desain</label>
                                                        <input type="file" class="form-control @error('desain') is-invalid @enderror" id="desain" name="desain"><br>
                                                        @error('desain')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- akhir Modal Edit -->


                        <form action="/admin/hapus/detail/{{$relasi->id}}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Yakin ingin menghapus pesanan ini ?')"><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>


                        <!-- ========================= download file ================================= -->


                        <!-- <td>
                <form action="{{$relasi->id}}" method="post">
                @csrf
                  <button type="submit">Download</button>
                </form>
              </td> -->


                        <!-- ========================================= akhir download file =========================== -->

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

