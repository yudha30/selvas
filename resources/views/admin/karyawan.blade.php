@extends ('template.mainAdmin')


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="container">
        <section class="content-header">
            <!-- <div class="container-fluid"> -->
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Admin</h1>
                </div>
                <!-- </div> -->
            </div><!-- /.container-fluid -->
        </section>
    </div>

    <!-- Main content -->


    <div class="container kontener">
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="row">
            <button class="btn btn-primary mb-2" type="button" data-toggle="modal" data-target="#exampleModal"
                id="open"> <i class="fas fa-plus"></i>&nbsp; Admin</button>

            <form class="form-inline mb-2 ml-auto" action="/admin/karyawan" method="GET">
                <input name="cari" id="cari" class="form-control mr-sm-2 " type="search" placeholder="Cari Admin"
                    aria-label="Search" value="{{ old('cari')}}">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i
                        class="fas fa-search"></i></button>
            </form>
            <!-- Modal Tambah-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Admin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/admin/store" method="post" enctype="multipart/form-data" id="formTambah">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Lengkap</label>
                                    <input name="name" id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{old('name')}}">
                                    @error('name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label>
                                    <input name="email" id="email" type="text"
                                        class="form-control @error('email') is-invalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail"
                                        value="{{old('email')}}">
                                    @error('email')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input name="password" id="password" type="password"
                                        class="form-control @error('password') isinvalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password">
                                    @error('password')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat</label>
                                    <textarea name="alamat" id="alamat"
                                        class="form-control @error('alamat') is-invalid @enderror"
                                        id="exampleFormControlTextarea1" rows="3">{{old('alamat')}}</textarea>
                                    @error('alamat')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Telepon</label>
                                    <input name="telepon" id="telepon" type="text"
                                        class="form-control @error('telepon') is-invalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Telepon"
                                        value="{{old('telepon')}}">
                                    @error('telepon')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">foto</label>
                                    <input name="foto" id="foto" type="file"
                                        class="form-control @error('foto') is-invalid @enderror"
                                        value="{{old('foto')}}"><br>
                                    <img id="preview" src="{{url('/admin/icon_gallery/gallery.png')}}" alt="your image"
                                        width="300px">

                                    <!-- <input name="foto" id="foto" type="file" class="form-control @error('foto') is-invalid @enderror"> -->
                                    @error('foto')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- akhir Modal Tambah -->
            <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="table-primary">
                        {{-- <th scope="col">No</th> --}}
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Telepon</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0;?>
                    @foreach ($admin as $a)
                    <?php $no++ ; ?>
                    <tr>
                        {{-- <th scope="row">{{$no}}</th> --}}
                        <td>{{$a->name}}</td>
                        <td>{{$a->email}}</td>
                        <td>{{$a->alamat}}</td>
                        <td>{{$a->telepon}}</td>
                        <td><img src="{{url('/admin/foto_admin/'.$a->foto)}}" alt="" width="50px" height="50px"></td>
                        <td>

                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                data-target="#editModal{{$a->id}}" data-id="{{$a->id}}" data-name="{{$a->name}}"
                                data-email="{{ $a->email }}" data-password="{{$a->password}}"
                                data-alamat="{{$a->alamat}}" data-telepon="{{$a->telepon}}" data-foto="{{$a->foto}}"><i
                                    class="far fa-edit"></i></button>

                            <!-- Modal Edit-->
                            <div class="modal fade" id="editModal{{$a->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">

                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Form Edit Admin</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <form action="{{ $a->id }}" method="post" enctype="multipart/form-data"
                                                id="formEdit">
                                                @method('patch')
                                                @csrf
                                                <div class="form-group">
                                                    <!-- <label for="exampleInputEmail1">ID</label> -->
                                                    <input name="id" id="id" type="hidden" class="form-control" readonly
                                                        value="{{$a->id}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nama Lengkap</label>
                                                    <input name="name" id="name" type="text"
                                                        class="form-control @error('name') is-invalid @enderror"
                                                        placeholder="Nama Lengkap" value="{{$a->name}}"
                                                        value="{{ old($a->name)}}">
                                                    @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">E-mail</label>
                                                    <input name="email" id="email" type="text"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        id="exampleInputEmail1" aria-describedby="emailHelp"
                                                        placeholder="E-mail" value="{{$a->email}}">
                                                    @error('email')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Alamat</label>
                                                    <textarea name="alamat" id="alamat"
                                                        class="form-control @error('alamat') is-invalid @enderror"
                                                        id="exampleFormControlTextarea1"
                                                        rows="3">{{$a->alamat}}</textarea>
                                                    @error('alamat')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Telepon</label>
                                                    <input name="telepon" id="telepon" type="text"
                                                        class="form-control @error('telepon') is-invalid @enderror"
                                                        id="exampleInputEmail1" aria-describedby="emailHelp"
                                                        placeholder="Telepon" value="{{$a->telepon}}">
                                                    @error('telepon')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Foto</label>
                                                    <input type="file" class="form-control" id="foto" name="foto"><br>
                                                    <img id="lihat" src="{{url('/admin/foto_admin/'.$a->foto)}}"
                                                        alt="your image" width="300px">
                                                    <!-- <div class="progress mt-1">
                            <div class="bar"></div >
                            <div class="percent">0%</div >
                          </div> -->
                                                    @error('foto')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                        </div>
                                        <div class="modal-footer">

                                            <a href="/ubah/password"
                                                style="color:#FFC107; padding-right:40%;text-decoration:none;">Ubah
                                                Password</a>

                                            <!-- @if (Route::has('password.request'))
                                          <a class="btn btn-link" href="{{ route('password.request') }}">
                                              {{ __('Forgot Your Password?') }}
                                          </a>
                                      @endif -->
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- akhir Modal Edit -->



                            <form action="{{ $a->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Yakin ingin menghapus admin tersebut ?')"><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>


                        <!-- ========================= download file ================================= -->


                        <!-- <td>
                <form action="{{$a->id}}" method="post">
                @csrf
                  <button type="submit">Download</button>
                </form>
              </td> -->


                        <!-- ========================================= akhir download file =========================== -->

                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        <div class="links">
            {{ $admin->links() }}
        </div>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
