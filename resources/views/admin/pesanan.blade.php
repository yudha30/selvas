@extends ('template.mainAdmin')

@section('header')
<link rel="stylesheet" href="">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pesanan</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->


    <div class="container kontener mb-4">
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="row">
            <!-- <button class="btn btn-primary mb-2" type="button" data-toggle="modal" data-target="#exampleModal"
                id="open"> <i class="fas fa-plus"></i>&nbsp; Admin</button> -->

            <form class="form-inline mb-2 ml-auto" action="/admin/pesanan/cari" method="get">
                <input name="cari" id="cari" class="form-control mr-sm-2 " type="search" placeholder="Cari Kode Pesanan"
                    aria-label="Search" value="{{ old('cari')}}" required>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i
                        class="fas fa-search"></i></button>
            </form>
            <!-- Modal Tambah-->
            <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Admin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/admin/store" method="post" enctype="multipart/form-data" id="formTambah">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Lengkap</label>
                                    <input name="name" id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{old('name')}}">
                                    @error('name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label>
                                    <input name="email" id="email" type="text"
                                        class="form-control @error('email') is-invalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail"
                                        value="{{old('email')}}">
                                    @error('email')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input name="password" id="password" type="password"
                                        class="form-control @error('password') isinvalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password">
                                    @error('password')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat</label>
                                    <textarea name="alamat" id="alamat"
                                        class="form-control @error('alamat') is-invalid @enderror"
                                        id="exampleFormControlTextarea1" rows="3">{{old('alamat')}}</textarea>
                                    @error('alamat')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Telepon</label>
                                    <input name="telepon" id="telepon" type="text"
                                        class="form-control @error('telepon') is-invalid @enderror"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Telepon"
                                        value="{{old('telepon')}}">
                                    @error('telepon')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">foto</label>
                                    <input name="foto" id="foto" type="file"
                                        class="form-control @error('foto') is-invalid @enderror"
                                        value="{{old('foto')}}"><br>
                                    <img id="preview" src="{{url('/admin/icon_gallery/gallery.png')}}" alt="your image"
                                        width="300px">
                                    @error('foto')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir Modal Tambah -->
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr class="table-primary">
                            <th scope="col">Kode Pesanan</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Telepon</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Email</th>
                            <th scope="col">Tanggal Pesan</th>
                            <th scope="col">Tanggal Selesai</th>
                            <th scope="col">Total Tagihan</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                        <tr>
                            <td><a href="/detail/pesanan/{{$d->id}}"> {{$d->id}} </a></td>
                            <td> <a href="/detail/pesanan/{{$d->id}}"> {{$d->nama_pemesan}} </a></td>
                            <td>{{$d->telepon}}</td>
                            <td>{{$d->alamat_kirim}}</td>
                            <td>{{$d->email}}</td>
                            <td>{{ date("d/m/Y h:i:s", strtotime($d->tgl_pemesanan))}}</td>
                            <td>{{date("d/m/Y", strtotime($d->tgl_selesai))}}</td>
                            <td>{{$d->total_harga}}</td>
                            <td>{{$d->keterangan}}</td>
                            <td>{{$d->status_pemesanan}}</td>
                            <td>
                                {{-- <img src="{{url('/admin/foto_admin/'.$d->foto)}}" alt="" width="50px" height="50px"> --}}

                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="#editModal{{$d->id}}" data-id="{{$d->id}}"
                                    data-keterangan="{{$d->keterangan}}" data-status="{{ $d->status_pemesanan }}"
                                    data-tgl_selesai="{{$d->tgl_selesai}}"><i class="far fa-edit"></i></button>

                                <!-- Modal Edit-->
                                <div class="modal fade" id="editModal{{$d->id}}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">

                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Form Edit Pesanan
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form action="/admin/edit/pesanan/{{$d->id}}" method="post"
                                                    id="formEdit">
                                                    @method('patch')
                                                    @csrf
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Keterangan</label>
                                                        <div class="col-sm-8">
                                                            <textarea type="text"
                                                                class="form-control @error('keterangan') is-invalid @enderror"         id="keterangan" name="keterangan" >{{$d->keterangan}}</textarea>
                                                            @error('keterangan')
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Status</label>
                                                        <div class="col-sm-8">
                                                            <select class="custom-select" name="status" id="status">
                                                                <option value="Pending" @if($d->status_pemesanan == "Pending") selected @endif>Pending</option>
                                                                <option value="Disetujui"@if($d->status_pemesanan == "Disetujui") selected @endif>Disetujui</option>
                                                                <option value="Potong" @if($d->status_pemesanan == "Potong") selected @endif>Potong</option>
                                                                <option value="Sablon/Bordir" @if($d->status_pemesanan == "Sablon/Bordir") selected @endif>Sablon/Bordir</option>
                                                                <option value="Jahit" @if($d->status_pemesanan == "Jahit") selected @endif>Jahit</option>
                                                                <option value="Packing" @if($d->status_pemesanan == "Packing") selected @endif>Packing</option>
                                                                <option value="Kirim" @if($d->status_pemesanan == "Kirim") selected @endif>Kirim</option>
                                                                <option value="Selesai" @if($d->status_pemesanan == "Selesai") selected @endif>Selesai</option>
                                                            </select>
                                                            @error('keterangan')
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </td>


                            <!-- ========================= download file ================================= -->


                            <!-- <td>
                    <form action="{{$d->id}}" method="post">
                    @csrf
                    <button type="submit">Download</button>
                    </form>
                </td> -->


                            <!-- ========================================= akhir download file =========================== -->

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="links">
            {{ $data->links() }}
        </div>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
