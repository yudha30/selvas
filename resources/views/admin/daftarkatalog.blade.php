@extends ('template.mainAdmin')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                <h1>Katalog {{$namaProduk->nama_produk}}</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="row ml-1">
        <button class="btn btn-primary mb-2 ml-3" type="button" data-toggle="modal" data-target="#exampleModal"
            id="open"> <i class="fas fa-plus"></i>&nbsp; {{$namaProduk->nama_produk}}</button>
        <!-- Modal Tambah-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Form Tambah {{$namaProduk->nama_produk}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/katalog/tambah" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Produk</label>
                                <select class="custom-select" name="produk">
                                    <option value="{{$namaProduk->id}}" selected>{{$namaProduk->nama_produk}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Keterangan</label>
                                <textarea name="keterangan" id="keterangan" type="text"
                                    class="form-control @error('keterangan') is-invalid @enderror"
                                    placeholder="Keterangan" cols="30" rows="10">{{old('keterangan')}}</textarea>
                                <br>
                                @error('keterangan')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Foto</label>
                                <input name="foto" id="foto" type="file"
                                    class="form-control @error('foto') is-invalid @enderror"
                                    value="{{old('foto')}}"><br>
                                @error('foto')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                                <img id="preview" src="{{url('/admin/icon_gallery/gallery.png')}}" alt="your image"
                                    width="300px">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- akhir Modal Tambah -->
    </div>
    <div class="row ml-3">
        @if(session('sukses'))
        <div class="alert alert-success col-4" role="alert">
            {{session('sukses')}}
        </div>
        @elseif(session('gagal'))
        <div class="alert alert-danger col-4" role="alert">
            {{session('gagal')}}
        </div>
        @endif
        <?php $cek=0; ?>
        @foreach ($errors->all() as $error)
        <?php
                $cek++;
            ?>
        @endforeach
        @if($cek>0)

        <div class="alert alert-danger col-6" role="alert">
            Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
        </div>
        <?php $cek=0; ?>
        @endif
    </div>
    <div class="col-12">
        <div class="card card-primary">
            <div class="card-header">
                <div class="card-title">
                    Daftar Katalog
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach ($data as $k)
                    <div class="col-sm-2">
                        <a href="{{url('/admin/katalog/'.$namaProduk->nama_produk.'/'.$k->foto)}}" data-toggle="lightbox"
                            data-title="{{$k->keterangan}}" data-gallery="gallery">
                            <div class="previewKatalog'.$namaProduk->nama_produk.'">
                                <img src="{{url('/admin/katalog/'.$namaProduk->nama_produk.'/'.$k->foto)}}" class="img-fluid mb-2"
                                    alt="white sample" />
                            </div>
                        </a>
                        <form action="/admin/katalog/{{$k->kd_katalog}}" method="post" class="d-inline">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger"
                                onclick="return confirm('Yakin ingin menghapus {{$k->keterangan}} ?')"
                                id="hapusKatalogKaos">Hapus</button>
                        </form>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </section>
    <!-- akhir data pelanggan -->


    <!-- /.content-wrapper -->
</div>

@endsection
