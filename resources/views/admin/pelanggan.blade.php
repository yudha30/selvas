@extends ('template.mainAdmin')


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Data Pelanggan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    
    
    
    <!-- tampil data pelanggan -->
    <section class="content-header">
      <div class="kontenerPelanggan">
          <!-- data table -->
    
              <table id="example1" class="table table-sm">
                <thead>
                <tr class="table-primary">
                  <th scope="col">Nama</th>
                  <th scope="col">Email</th>
                  <th scope="col">Provider</th>
                  
                </tr>
                </thead>
                <tbody>
                @foreach ($pelanggan as $p)
                <tr>
                  <td>{{$p->name}}</td>
                  <td>{{$p->email}}</td>
                  <td>{{$p->provider}}</td>
                </tr>
                @endforeach
                
                </tbody>
              </table>
            
    <!-- akhir data table -->
        </div>
      </section>
    <!-- akhir data pelanggan -->
    
    
    
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection