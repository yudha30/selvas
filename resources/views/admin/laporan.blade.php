@extends ('template.mainAdmin')


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Pemesanan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="container kontener">
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="row">
            <form action="/cetak/laporan" method="post">
                @csrf
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-primary" style="width:130px !important;">Cetak Laporan</button>
                    </div>
                    <div class="col">
                        <input type="hidden" class="form-control mr-3" name="bulan" style="width:150px !important;" value="{{$bulan}}" readonly>
                    </div>
                    <div class="col">
                        <input type="hidden" class="form-control mr-3" name="tahun" style="width:150px !important;" value="{{$tahun}}" readonly>
                    </div>
                </div>
            </form>
            <form class="form-inline mb-2 ml-auto" action="/cari/laporan" method="POST">
                @csrf
                Dari Tanggal :&nbsp;
                <input type="date" class="form-control mr-2" id="bulan" name="bulan" placeholder="Dari Tanggal" value="{{old('bulan')}}" required>
                Sampai : &nbsp;
                <input type="date" class="form-control mr-2" id="tahun" name="tahun" placeholder="Sampai Tanggal" value="{{old('tahun')}}" required>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i
                        class="fas fa-search"></i></button>
            </form>

            <table class="table table-sm">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">Nama Pemesan</th>
                        <th scope="col">Kode Pemesan</th>
                        <th scope="col">Tanggal Selesai</th>
                        <th scope="col">Status Pemesanan</th>
                        <th scope="col">Total Harga</th>
                        <th scope="col">Status Pembayaran</th>
                        <th scope="col">Total Bayar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($relasi as $r)
                    <tr>
                        <td>{{$r->nama_pemesan}}</td>
                        <td>{{$r->pesanan_id}}</td>
                        <td>{{date('d F Y', strtotime($r->tgl_selesai))}}</td>
                        <td>{{$r->status_pemesanan}}</td>
                        <td>{{$r->total_harga}}</td>
                        <td>{{$r->status_pembayaran}}</td>
                        <td>{{$r->total_bayar}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="links">
            {{ $relasi->links() }}
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
