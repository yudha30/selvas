<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .header{
            font-family: arial, sans-serif;
            font-size: 18px;
            margin-bottom: 5%;
        }
        table.data{
            font-family: arial, sans-serif;
            border-collapse: collapse;
            border: 1px solid #dddddd;
        }

        table.total{
            font-family: arial, sans-serif;
            /* border-collapse: collapse; */
            margin-top: 3%;
        }
        .footer{
            float: right;
        }
        .ttd{
            /* margin-right: 3% */
            padding-right: 5%;
            width: 25%;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
        <center> <strong> Laporan CV Selvas <br> Dari Tanggal : {{date('d F Y', strtotime($bulan))}} Sampai : {{date('d F Y', strtotime($tahun))}}</strong></center>
        </div>
        <div class="content" style="font-size:12px;">
            <table border="1" class="data">
                <thead>
                    <tr>
                        <th align="center">Nama Pemesan</th>
                        <th align="center">Kode Pemesan</th>
                        <th align="center">Tanggal Selesai</th>
                        <th align="center">Status Pemesanan</th>
                        <th align="center">Total Harga</th>
                        <th align="center">Status Pembayaran</th>
                        <th align="center">Total Bayar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total_harga=0; $total_bayar=0; ?>
                    @foreach($relasi as $r)
                        <?php
                            $total_harga=$total_harga+$r->total_harga;
                            $total_bayar=$total_bayar+$r->total_bayar;
                        ?>
                    <tr>
                        <td>{{$r->nama_pemesan}}</td>
                        <td>{{$r->pesanan_id}}</td>
                        <td>{{date('d F Y', strtotime($r->tgl_selesai))}}</td>
                        <td>{{$r->status_pemesanan}}</td>
                        <td>{{$r->total_harga}}</td>
                        <td>{{$r->status_pembayaran}}</td>
                        <td>{{$r->total_bayar}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <table border="0" class="total">
                <tr>
                    <td>Total Harga</td>
                    <td>:</td>
                    <th>{{$total_harga}}</th>
                </tr>
                <tr>
                    <td>Total Pembayaran</td>
                    <td>:</td>
                    <th>{{$total_bayar}}</th>
                </tr>
            </table>
        </div>
        <div class="footer">
            <div class="ttd">
                Direktur
                <br>
                <br>
                <br>
                Iskandarsyah Putra
            </div>
        </div>
    </div>
</body>
</html>
