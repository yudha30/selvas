@extends ('template.mainAdmin')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Data Produk</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <div class="row ml-3">
        @if(session('sukses'))
        <div class="alert alert-success col-4" role="alert">
            {{session('sukses')}}
        </div>
        @elseif(session('gagal'))
        <div class="alert alert-danger col-4" role="alert">
            {{session('gagal')}}
        </div>
        @endif
        <?php $cek=0; ?>
        @foreach ($errors->all() as $error)
        <?php
                $cek++;
            ?>
        @endforeach
        @if($cek>0)

        <div class="alert alert-danger col-6" role="alert">
            Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
        </div>
        <?php $cek=0; ?>
        @endif
    </div>
    <div class="row ml-1">
        <button class="btn btn-primary mb-2 ml-3" type="button" data-toggle="modal" data-target="#exampleModal"
            id="open"> <i class="fas fa-plus"></i>&nbsp; Produk</button>
        <!-- Modal Tambah-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Form Tambah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/produk/tambah" method="post" enctype="multipart/form-data" id="formTambah">
                            @csrf

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Produk</label>
                                <input name="nama" id="nama" type="text"
                                    class="form-control @error('nama') is-invalid @enderror" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="Nama Produk" value="{{old('nama')}}">
                                @error('nama')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Bahan</label>
                                <table>
                                    @foreach ($bahan as $b)
                                    <tr>
                                        <td>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <input type="checkbox" id="cek"
                                                            aria-label="Checkbox for following text input"
                                                            name="select[]" value="{{$b->id}}"
                                                            class="@error('cek') is-invalid @enderror">
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control"
                                                    aria-label="Text input with checkbox" disabled value="{{$b->nama_bahan}}">
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                                @error('cek')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Foto</label>
                                <input name="foto" id="foto" type="file"
                                    class="form-control @error('foto') is-invalid @enderror"
                                    value="{{old('foto')}}"><br>
                                @error('foto')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                                <img id="preview" src="{{url('/admin/icon_gallery/gallery.png')}}" alt="your image"
                                    width="300px">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- akhir Modal Tambah -->
    </div>

    <!-- tampil data produk -->
    <section class="content-header">
        <div class="kontenerPelanggan">
            <!-- data table -->
            <div class="table-responsive">
            <table id="example1" class="table table-sm">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">Nama</th>
                        <th scope="col">Gambar Produk</th>
                        <th scope="col">Bahan</th>
                        <th scope="col">Aksi</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $p)
                    <tr>
                        <td>{{$p->nama_produk}}</td>
                        <td><img src="{{url('/admin/produk/'.$p->gambar_produk)}}" alt="" width="100px" height="100px">
                        </td>
                        <td>
                            <?php
                        $gabung="";
                    ?>
                            @foreach ($p->bahan as $data_bahan)
                            <!-- {{ $data_bahan->nama}} -->
                            <?php
                            $gabung=$gabung.$data_bahan->nama_bahan.", ";
                        ?>
                            @endforeach
                            {{$gabung}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                data-target="#editModal{{$p->id}}" data-id="{{$p->id}}" data-name="{{$p->nama_produk}}"
                                data-foto="{{$p->gambar_produk}}"><i class="far fa-edit"></i></button>

                            <!-- Modal Edit-->
                            <div class="modal fade" id="editModal{{$p->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">

                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Form Edit Produk</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('editproduk')}}"  method="post"
                                            onSubmit="return check()" enctype="multipart/form-data" id="formEdit" >
                                                @method('patch')
                                                @csrf
                                                <div class="form-group">
                                                    <input name="id" id="id" type="hidden" class="form-control" readonly
                                                        value="{{$p->id}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nama Lengkap</label>
                                                    <input name="nama" id="nama" type="text"
                                                        class="form-control @error('nama') is-invalid @enderror"
                                                        placeholder="Nama Lengkap" value="{{$p->nama_produk}}"
                                                        value="{{ old($p->nama_produk)}}">
                                                    @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Bahan</label>
                                                    <table>
                                                        @foreach ($bahan as $b)
                                                        <tr>
                                                            <td>
                                                                <div class="input-group mb-3">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">

                                                                            <input type="checkbox" id="cek"
                                                                                aria-label="Checkbox for following text input"
                                                                                name="select[]" value="{{$b->id}}"
                                                                                class="@error('cek') is-invalid @enderror">
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                        aria-label="Text input with checkbox" disabled
                                                                        value="{{$b->nama_bahan}}">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                    @error('cek')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Foto</label>
                                                    <input type="file" class="form-control" id="foto" name="foto"><br>
                                                    <img id="lihat" src="{{url('/admin/produk/'.$p->gambar_produk)}}"
                                                        alt="your image" width="300px">
                                                    @error('foto')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- akhir Modal Edit -->



                            <form action="/admin/produk/{{$p->id}}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Yakin ingin menghapus admin tersebut ?')"><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            </div>
            <!-- akhir data table -->
        </div>
    </section>
    <!-- akhir data pelanggan -->





    <!-- /.content-wrapper -->
</div>



@endsection


