@extends ('template.mainAdmin')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Data Bahan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

        <div class="row ml-3">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
            {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
            {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
                $cek++;
            ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
            Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif
        </div>
        <div class="row ml-1">
            <button class="btn btn-primary mb-2 ml-3" type="button" data-toggle="modal" data-target="#exampleModal" id="open"> <i class="fas fa-plus"></i>&nbsp; Bahan</button>
            <!-- Modal Tambah-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Form Tambah Bahan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/bahan/tambah" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Bahan</label>
                            <input name="nama" id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Produk" value="{{old('nama')}}">
                            @error('nama')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" type="text" class="form-control @error('keterangan') is-invalid @enderror" placeholder="Keterangan" cols="30" rows="10">{{old('keterangan')}}</textarea>
                            <br>
                            @error('keterangan')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submit">Tambah</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
            <!-- akhir Modal Tambah -->
        </div>

    <!-- tampil data produk -->
    <section class="content-header">
      <div class="kontenerPelanggan">
        <div class="table-responsive">
          <!-- data table -->

              <table id="example1" class="table table-sm">
                <thead>
                <tr class="table-primary">
                  <th scope="col" width="150px">Nama</th>
                  <th scope="col">Keterangan</th>
                  <th scope="col" width="70px">Aksi</th>

                </tr>
                </thead>
                <tbody>
                @foreach ($data as $d)
                <tr>
                  <td>{{$d->nama_bahan}}</td>
                  <td>{{$d->keterangan}}</td>
                  <td>
                  <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$d->id}}"  data-id="{{$d->id}}" data-name="{{$d->nama_bahan}}" data-foto="{{$d->gambar_bahan}}" title="Edit"><i class="far fa-edit"></i></button>

                    <!-- Modal Edit-->
                    <div class="modal fade" id="editModal{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Edit Bahan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">

                            <form action="/admin/bahan/{{$d->id}}/edit" method="post">
                                @method('patch')
                                @csrf
                                <div class="form-group">
                                    <!-- <label for="exampleInputEmail1">ID</label> -->
                                    <input name="id" id="id" type="hidden" class="form-control" readonly value="{{$d->id}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Lengkap</label>
                                    <input name="nama" id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Lengkap" value="{{$d->nama_bahan}}" value="{{ old($d->nama_bahan)}}">
                                    @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Keterangan</label>
                                    <textarea name="keterangan" id="keterangan" type="text" class="form-control @error('keterangan') is-invalid @enderror" placeholder="Keterangan" cols="30" rows="10">{{$d->keterangan}}</textarea>
                                    <br>
                                    @error('keterangan')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!-- akhir Modal Edit -->



                    <form action="/admin/bahan/{{$d->id}}/hapus" method="post" class="d-inline">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ingin menghapus bahan tersebut ?')" title="Hapus"><i class="fas fa-trash-alt"></i></button>
                    </form>
                  </td>
                </tr>
                @endforeach

                </tbody>
              </table>

    <!-- akhir data table -->
        </div>
        </div>
      </section>
    <!-- akhir data pelanggan -->


  <!-- /.content-wrapper -->
  </div>

@endsection
