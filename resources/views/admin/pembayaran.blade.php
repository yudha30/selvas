@extends ('template.mainAdmin')


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pembayaran</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="container kontener">
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-4" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-4" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <?php $cek=0; ?>
            @foreach ($errors->all() as $error)
            <?php
            $cek++;
          ?>
            @endforeach
            @if($cek>0)

            <div class="alert alert-danger col-6" role="alert">
                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
            </div>
            <?php $cek=0; ?>
            @endif

        </div>
        <div class="row">

            <div class="col-3 d-flex justify-content-between">
                <span class="pt-2"> Cari</span> &nbsp;&nbsp; <input type="date" class="form-control "
                    id="cari_tgl_bayar" name="cari_tgl_bayar" placeholder="Dari Tanggal" value="{{old('bulan')}}"
                    onchange="return caritanggal()">
            </div>
            <form class="form-inline mb-2 ml-auto" action="/pembayaran/cari" method="POST">
                @csrf
                <input name="cari" id="cari" class="form-control mr-sm-2 " type="search" placeholder="Cari Kode Pesanan"
                    aria-label="Search" value="{{ old('cari')}}">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i
                        class="fas fa-search"></i></button>
            </form>
            <div class="table-responsive">


                <table class="table table-sm">
                    <thead>
                        <tr class="table-primary">
                            {{-- <th scope="col">No Pembayaran</th> --}}
                            <th scope="col">Kode Pemesanan</th>
                            <th scope="col">Nama Pemesan</th>
                            {{-- <th scope="col">Bukti Pembayaran</th> --}}
                            <th scope="col">Update Pembayaran</th>
                            <th scope="col">Total Tagihan</th>
                            <th scope="col">Jumlah Bayar</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pembayaran as $a)
                        <tr>
                            {{-- <td>{{$a->id}}</td> --}}
                            {{-- <td>{{$a->pesanan_id}}</td>
                            <td>{{$a->pesanan['nama_pemesan']}}</td> --}}
                            <td><a href="/cari/pesanan/{{$a->pesanan_id}}"> {{$a->pesanan_id}} </a></td>
                            <td> <a href="/cari/pesanan/{{$a->pesanan_id}}"> {{$a->pesanan['nama_pemesan']}} </a></td>
                            {{-- <td>
                                <a href="{{url('/admin/bukti_pembayaran/'.$a->bukti_pembayaran)}}"
                            data-toggle="lightbox" data-title="Bukti Pembayaran" data-gallery="gallery">
                            <div class="previewKatalogKaos">
                                <img src="{{url('/admin/bukti_pembayaran/'.$a->bukti_pembayaran)}}"
                                    class="img-fluid mb-2" alt="Belum ada bukti bayar" width="60px" height="60px"
                                    style="color:black;" />
                            </div>
                            </a>
                            </td> --}}
                            <td>{{date("d/m/Y H:i:s", strtotime($a->updated_at))}}</td>
                            <td>{{$a->pesanan['total_harga']}}</td>
                            <td>{{$a->total_bayar}}</td>
                            <td>{{$a->status_pembayaran}}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-sm" title="Detail Pembayaran" id="btnDetail"
                            data-toggle="modal" data-target="#DetailModalCenter" data-id="{{$a->id}}" onclick="getDetail({{$a->id}})"><i class="fas fa-clipboard-list"></i></button>
                                <!-- Modal Detail-->
                                <div class="modal fade" id="DetailModalCenter" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Detail Pembayaran </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div id="data">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                {{-- <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="#editModal{{$a->id}}" data-id="{{$a->id}}"
                                data-jumlah_pembayaran="{{$a->jumlah_pembayaran}}"
                                data-status="{{$a->status_pembayaran}}"><i class="far fa-edit"></i></button> --}}

                                <!-- Modal Edit-->
                                {{-- <div class="modal fade" id="editModal{{$a->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">

                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Form Edit Pembayaran
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/admin/edit/pembayaran/{{$a->id}}" method="post"
                                                id="formEdit">
                                                @method('patch')
                                                @csrf
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Jumlah Pembayaran</label>
                                                    <div class="col-sm-8">
                                                        <input type="text"
                                                            class="form-control @error('jml_bayar') is-invalid @enderror"
                                                            id="jml_bayar" name="jml_bayar"
                                                            value="{{$a->jumlah_pembayaran}}">
                                                        @error('jml_bayar')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Status</label>
                                                    <div class="col-sm-8">
                                                        <select class="custom-select" name="status" id="status">
                                                            <option value="Belum Lunas" @if($a->status_pembayaran ==
                                                                "Belum Lunas") selected @endif>Belum Lunas</option>
                                                            <option value="Lunas" @if($a->status_pembayaran ==
                                                                "Lunas") selected @endif>Lunas</option>
                                                        </select>
                                                        @error('status')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
            </div>



            <form action="/hapus/pembayaran/{{$a->id}}" method="post" class="d-inline">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger btn-sm"
                    onclick="return confirm('Yakin ingin menghapus pembayaran tersebut ?')"><i
                        class="fas fa-trash-alt"></i></button>
            </form> --}}
            </td>


            <!-- ========================= download file ================================= -->


            <!-- <td>
                <form action="{{$a->id}}" method="post">
                @csrf
                  <button type="submit">Download</button>
                </form>
              </td> -->


            <!-- ========================================= akhir download file =========================== -->

            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>
    <div class="links">
        {{ $pembayaran->links() }}
    </div>
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@section('javascript_admin')

<script>
    function caritanggal() {
        let tgl = document.getElementById("cari_tgl_bayar").value;
        if (tgl) {
            window.location.href = "/cari/tanggal/" + tgl;
        }
    };

    function getDetail($idPembayaran){
        let idPembayaran=$idPembayaran;

        $.ajax({
            type: 'get',
            url: '/admin/getDetailPembayaran/'+idPembayaran,
            success: function(response) {
                // console.log(response);
                $('#data').html('');
                $('#data').append(response);
            }

        });
    };

</script>

@endsection
