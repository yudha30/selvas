@component('mail::message')
# Pembayaran

Status pembayaran anda : <strong>{{$data}}</strong> <br>
<span style="color:rgba(0, 0, 0, 0.705);"> Silahkan login untuk melihat transaksi anda. <br>
Terimakasih sudah mempercayakan kami sebagai produsen pakaian. Kami tunggu pesanan anda berikutnya.</span>
{{-- @component('mail::button', ['url' => '127.0.0.1:8000/login'])
Klik Disini
@endcomponent --}}

<div class="hormat" style="text-align:right; font-size:15px;">
Hormat Kami,<br><br>
CV. Selvas
</div>
@endcomponent
