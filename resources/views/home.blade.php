    @extends('template.main')

    @section('title','Home')

    @section('content')
    <!-- carousel -->

    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{url('/assets/img/carousel-1.jpg')}}" class="d-block w-100 img-fluid">
                <div class="carousel-caption d-xs-block">
                    <span class="text-jargon">Jangan Ragu Pilih Kami</span>
                    <p class="text-keterangan">Bekerja Profesional, cepat, dan Kualitas Mutu Terjamin.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{url('/assets/img/carousel-2.jpg')}}" class="d-block w-100 img-fluid">
                <div class="carousel-caption d-xs-block">
                    <span class="text-jargon">Kualitas Produk Unggulan</span>
                    <p class="text-keterangan">Dibuat Dari Bahan Terbaik dan Didukung Dengan Peralatan Yang Canggih.</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- akhir carousel -->


    <!-- visi misi -->
    <div class="container">
        <div class="row visimisi">
            <div class="col mt-5">
                <h2 class="text-center mt-4 kenapa" data-aos="fade-right">Kenapa Memilih Kami ?</h2>
            </div>
            <div class="col mt-5 visi" data-aos="fade-left" data-aos-delay="200">
                <ol type="1">
                    <li>Perusahaan kami telah berpengalaman selama belasan tahun dibidang industri konveksi.</li>
                    <li>Legalitas perusahaan sangat lengkap dan telah dipercaya oleh rekanan kami.</li>
                    <li>Menghasilkan produk dengan kualitas terbaik.</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- akkhir visi misi -->

    <div class="keunggulan">
        <div class="row text-center produk pt-4 mt-5 pb-4">
            <div class="col">
                <h2 style="color:white;">Keunggulan Memesan Ditempat Kami</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in">
                <img src="{{url('/assets/img/icon/badan_hukum.png')}}" width="55"><br>
                <div class="keterangan_keunggulan pt-3">
                    Berbadan Hukum
                </div>
                <div class="keterangan pt-3">
                    Legalitas perusahaan resmi dari pemerintah dengan nama CV. SELVAS.
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in" data-aos-delay="500">
                <img src="{{url('/assets/img/icon/paket.png')}}" width="100"><br>
                <div class="keterangan_keunggulan pt-3">
                    Pengiriman Seluruh Indonesia
                </div>
                <div class="keterangan pt-3">
                    Bekerja sama dengan perusahaan ekspedisi profesional.
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in" data-aos-delay="1000">
                <img src="{{url('/assets/img/icon/desain.png')}}" width="110"><br>
                <div class="keterangan_keunggulan pt-3">
                    Desain Gratis
                </div>
                <div class="keterangan pt-3">
                    Kami memberikan layanan desain gratis bagi yang membutuhkan.
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in" data-aos-delay="1500">
                <img src="{{url('/assets/img/icon/produk.png')}}" width="70"><br>
                <div class="keterangan_keunggulan pt-3">
                    Kualitas Produk Terjamin
                </div>
                <div class="keterangan pt-3">
                    Kami menggunakan bahan unggulan dengan mutu terbaik.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in">
                <img src="{{url('/assets/img/icon/telpon.png')}}" width="70"><br>
                <div class="keterangan_keunggulan pt-3">
                    Layanan Pelanggan
                </div>
                <div class="keterangan pt-3">
                    Layanan admin dapat dikirim melaui Email maupun Whatsapp pada saat jam kerja.
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in" data-aos-delay="500">
                <img src="{{url('/assets/img/icon/dompet.png')}}" width="75"><br>
                <div class="keterangan_keunggulan pt-3">
                    Harga Terjangkau
                </div>
                <div class="keterangan pt-3">
                    Biaya yang optimal untuk hasil produk yang maksimal.
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in" data-aos-delay="1000">
                <img src="{{url('/assets/img/icon/proses.png')}}" width="75"><br>
                <div class="keterangan_keunggulan pt-3">
                    Pantau Proses
                </div>
                <div class="keterangan pt-3">
                    Pemberitahuan setiap proses produksi akan dikirim melalui email pelanggan.
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center mt-5" data-aos="zoom-in" data-aos-delay="1500">
                <img src="{{url('/assets/img/icon/jam.png')}}" width="75"><br>
                <div class="keterangan_keunggulan pt-3">
                    Tepat Waktu
                </div>
                <div class="keterangan pt-3">
                    Terjadwal dan terstruktur berdasarkan kesepakatan waktu yang telah ditentukan.
                </div>
            </div>
        </div>
    </div>
    {{-- <hr class="mt-5"> --}}
    <div class="keunggulan">
        <div class="row text-center produk pt-4 mt-5 pb-4">
            <div class="col">
                <h2 style="color:white;">Produk</h2>
            </div>
        </div>
    </div>
    <!-- produk -->
    <div class="container mt-5">
        <div class="row justify-content-center mt-3">
            <div class="col-10">
                <div class="row">
                    <div class="col text-center mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/produk/kaos/kaos2.jpg')}}" height="70px"><br>
                        <b>Kaos</b>
                    </div>
                <div class="col text-center" data-aos="fade-up" data-aos-delay="100">
                    <img src="{{url('/assets/img/produk/topi/topi1.jpg')}}" height="70px"><br>
                    <b>Topi</b>
                </div>
                <div class="col text-center" data-aos="fade-up" data-aos-delay="200">
                    <img src="{{url('/assets/img/jaket.jpeg')}}" height="70px"><br>
                    <b>Jaket</b>
                </div>
                <div class="col text-center" data-aos="fade-up" data-aos-delay="300">
                    <img src="{{url('/assets/img/produk/kemeja/kemeja1.jpg')}}" height="70px"><br>
                    <b>Kemeja</b>
                </div>
                <div class="col text-center" data-aos="fade-up" data-aos-delay="400">
                    <img src="{{url('/assets/img/blazer.png')}}" height="70px"><br>
                    <b>Blazer</b>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center" data-aos="fade-up" data-aos-delay="500">
        <div class="col-12">
            <small class="text-muted"> Dan lain sebagainya. </small>
        </div>
    </div>
    </div>
    <!-- akhir produk -->
    <div>
        <div class="row text-center banner pt-5 mt-5">
            <div class="col">
                <h2 style="font-size:50px">Ayo Kerjasama Dengan CV Selvas</h2>
            </div>
        </div>
        <div class="row text-center mt-4" style="background-color:white;">
            <div class="col">
                <img src="{{url('/assets/img/icon/jabat.png')}}" width="200">
            </div>
        </div>
    </div>
    {{-- <hr class="mt-5"> --}}
    <div class="keunggulan">
        <div class="row text-center produk pt-4 mt-5 pb-4">
            <div class="col">
                <h2 style="color:white;">Kerjasama</h2>
            </div>
        </div>
    </div>
    <!-- Kerjasama -->
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12">
                <div class="row text-center mt-5">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/kerjasama/kai.png')}}" width="150px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4  mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/kerjasama/bpd-diy.png')}}" width="50px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/kerjasama/polda-DIY.png')}}" width="70px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/kerjasama/aau.png')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/kerjasama/uin-suka.png')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up">
                        <img src="{{url('/assets/img/kerjasama/logo-gapura.png')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="300">
                        <img src="{{url('/assets/img/kerjasama/sidomuncul.png')}}" width="150px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="300">
                        <img src="{{url('/assets/img/kerjasama/ugm.png')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="300">
                        <img src="{{url('/assets/img/kerjasama/kejaksaan.png')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="300">
                        <img src="{{url('/assets/img/kerjasama/kanwil-pajak.jpg')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="300">
                        <img src="{{url('/assets/img/kerjasama/lambang-unsyiah.png')}}" width="80px" height="80"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="300">
                        <img src="{{url('/assets/img/kerjasama/wk.png')}}" width="80px" height="80px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="500">
                        <img src="{{url('/assets/img/kerjasama/yonif-403.png')}}" width="80px" height="80px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="500">
                        <img src="{{url('/assets/img/kerjasama/smanps.png')}}" width="80px" height="90px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="500">
                        <img src="{{url('/assets/img/kerjasama/serambi mekkah.png')}}" width="80px" height="80px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="500">
                        <img src="{{url('/assets/img/kerjasama/malukussaleh.png')}}" width="80px" height="80px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="500">
                        <img src="{{url('/assets/img/kerjasama/banjarnegara.png')}}" width="60px" height="80px"><br>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-4 mb-3" data-aos="fade-up" data-aos-delay="500">
                        <img src="{{url('/assets/img/kerjasama/syariat islam.png')}}" width="60px" height="80px"><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <small class="text-muted text-center"> Dan lain sebagainya. </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- akhir kerjasama width="70px" height="80px"-->

    @endsection
