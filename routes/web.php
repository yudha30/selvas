<?php
use App\Events\FormNotification;
use App\Http\Controllers\PesananController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

// =============== load menu_produk ==============
route::get('/load/menu_produk','ProdukController@menu_produk');
// =============== load menu_produk ==============


//================ show produk ===================
route::get('/produk/{id}','ProdukController@sortir_menu_produk');
//================ show produk ===================

//================ show produk ===================

//================ show produk ===================



route::get('/kaos','ProdukController@kaos');
route::get('/kemeja','ProdukController@Kemeja');
route::get('/pesan','PesananController@tampilpesan');
route::get('/jaket','JaketController@show');
route::get('/topi','TopiController@show');
route::get('/blazer','BlazerController@show');

// cari produk
route::get('/cari/produk/{id}', 'ProdukController@cari');

// ========================== admin =========================
route::group(['middleware' => 'auth:admin'],function(){
    // tambahan baru
    route::get('/load/list_produk','ProdukController@list_produk');
    route::get('/admin/daftarkatalog/{id}','KatalogController@show');
    // akhir
    route::get('/admin/dashboard', 'DashboardController@index');

    //admin
    route::get('/admin/karyawan', 'AdminController@index');//index
    route::patch('/admin/{admin}', 'AdminController@update');//edit blm jadi
    route::delete('/admin/{admin}', 'AdminController@destroy');//delete
    route::post('/admin/store', 'AdminController@store');//simpan


    route::get('/admin/pelanggan', 'AdminController@pelanggan');
    route::get('/admin/pesanan', 'PesananController@index');
    route::get('/admin/pembayaran', 'PembayaranController@index');
    // route::get('/admin/pembayaran', 'AdminController@pembayaran');
    route::get('/admin/laporan', 'LaporanController@laporan');
    route::post('/admin/logout', 'Auth\LoginController@logout');
    route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
    route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

    route::get('/ubah/password', 'LupaPasswordController@tampilubah');
    route::post('/ubah/password/simpan', 'LupaPasswordController@update');

    // ================ route Pesanan ==========================

    route::get('/admin/pesanan/cari','PesananController@index');
    route::patch('/admin/edit/pesanan/{id}' ,'PesananController@update');
    route::delete('/admin/hapus/pesanan/{id}', 'PesananController@destroy');
    route::get('/download/desain/{id}', 'PesananController@unduh');//download file JADII!!

    // ================ akhir Pesanan ==========================

    // ================ detail pemesanan =======================

    route::get('/detail/pesanan/{id}', 'PesananController@lihat_detail');
    route::delete('/admin/hapus/detail/{id}', 'PesananController@hapus_detail');
    route::patch('/edit/detail/pesanan/{id}', 'PesananController@edit_detail_pesanan');

    // ================ akhir pemesanan =======================


    // ================ route produk ===========================

    route::get('/admin/tampilproduk','ProdukController@daftarproduk')->name('tampilproduk');
    route::post('admin/produk/tambah', 'ProdukController@store');
    route::patch('/admin/produk/edit' ,'ProdukController@update')->name('editproduk');
    route::delete('/admin/produk/{produk}', 'ProdukController@destroy');//delete

    // ================ akhir route produk ======================

    // ================ get Detail Pembayaran =======================
    route::get('/admin/getDetailPembayaran/{id}', 'PembayaranController@getDetailPembayaran');
    // ================ akhir get Detail Pembayaran =================

    // ================= route bahan ================================

    route::get('/admin/bahan', 'BahanController@index');
    route::post('/admin/bahan/tambah', 'BahanController@store');
    route::patch('/admin/bahan/{data}/edit', 'BahanController@update');
    route::delete('/admin/bahan/{data}/hapus', 'BahanController@destroy');

    // ================= akhr route bahan ===========================


    // ================= route katalog kaos ========================

    route::get('/admin/daftarkatalog', 'KaosController@index');
    route::post('/admin/katalog/tambah', 'KaosController@store');
    route::delete('/admin/katalog/{kd}', 'KaosController@destroy');

    // ================= akhir route katalog kaos ==================

    // ================= route katalog kemeja ======================

    route::get('/admin/tampil_kemeja', 'KemejaController@index');
    route::post('/admin/katalog/kemeja/tambah', 'KemejaController@store');
    route::delete('/admin/katalog/kemeja/{id}', 'KemejaController@destroy');

    // ================= akhir route katalog  ======================


    // ======================= katalog jaket ============================
    route::get('/admin/jaket', 'JaketController@index');
    route::post('/admin/tambah/jaket', 'JaketController@store');
    route::delete('/admin/katalog/jaket/{id}', 'JaketController@destroy');
    // ======================= akhir katalog jaket ======================


    // ======================= katalog topi ================================
    route::get('/admin/topi', 'TopiController@index');
    route::post('/admin/tambah/topi', 'TopiController@store');
    route::delete('/admin/katalog/topi/{id}', 'TopiController@destroy');
    // ======================= akhir katalog topi ================================


    // =============================== route katalog blazer ===================================
    route::get('/admin/blazer','BlazerController@index');
    route::post('/admin/tambah/blazer','BlazerController@store');
    route::delete('/admin/katalog/blazer/{id}','BlazerController@destroy');
    // =============================== akhir kalatog blazer ===================================


    // ==================== pembayaran =============================

    route::patch('/admin/edit/pembayaran/{id}','PembayaranController@update');
    route::post('/pembayaran/cari', 'PembayaranController@cari');
    route::delete('/hapus/pembayaran/{id}', 'PembayaranController@destroy');
    route::get('/cari/pesanan/{id}','PembayaranController@cari_pesanan');
    route::get('/cari/tanggal/{tgl}','PembayaranController@caritanggal');

    // ==================== akhir pembayaran =======================

    // ===================== Laporan ===============================

    route::post('/cari/laporan', 'LaporanController@cari');
    route::post('/cetak/laporan','LaporanController@cetak');

    // ===================== akhir laporan =========================

    Auth::routes(['login'=> false,'password'=>false]);


    // ===================== ukuran ================================
    route::get('/admin/ukuran', 'UkuranController@index')->name('IndexUkuran');
    route::post('/admin/ukuran', 'UkuranController@store');
    route::patch('/admin/ukuran/{id}', 'UkuranController@update');
    route::delete('/admin/ukuran/{id}', 'UkuranController@destroy');
    // ===================== akhir ukuran ==========================


});


route::get('/login','Auth\LoginController@showLoginForm')->name('login');
route::post('/login','Auth\LoginController@login');

// ========================== Login =========================
// Route::get('/login/admin', 'Auth\LoginController@tampillogin');
// Route::get('/home', 'HomeController@index')->name('home');




//============================ Pelanggan ====================
route::get('/pelanggan', 'PelangganController@index');
//============================ akhir pelanggan ==============





// route login with google
// ========================== login google ==================
route::get('auth/{provider}', 'PelangganController@redirectToProvider');
route::get('auth/{provider}/callback', 'PelangganController@handleProviderCallback');
// ========================== akhir login google ============


route::post('/logout/user', 'PelangganController@logout');



// ========================== transaksi user ======================
route::group(['middleware' => 'auth:pelanggan'],function(){
    // ========================== FormNotification ==============
    // route::post('/pesanan', function(){
        //kirim notif


        // $text='Ada Pemesanan Baru. Silahkan Cek Pesanan!';
        // event(new FormNotification($text));
    // });

    route::post('/pesanan/selesai/{id}', 'TransaksiController@notif');
    route::get('/notif_pembayaran', 'TransaksiController@notif_pembayaran');
    // ========================== Akhir FormNotification ==============

    route::get('/transaksi', 'TransaksiController@lihat_transaksi');
    route::patch('/bukti/pembayaran/{id}', 'TransaksiController@upload_pembayaran');
    route::post('/cetak/transaksi/{id}', 'TransaksiController@cetak_transaksi');

    // ================ hapus pesanan dari pelanggan ============

    route::delete('/hapus/detail/pesanan/{id}', 'PesananController@hapus_pesan_pelanggan');

    // ================ akhir hapus pesanan dari pelanggan ============

    route::post('/pesanan/tambah', 'PesananController@store');
    route::patch('/pesanan/tambah_pemesanan', 'PesananController@tambah_pemesanan');
    route::get('/keranjang', 'PesananController@tampilkeranjang');

    // ============================= midtrans =============================
    Route::post('/finish', function(){
        return redirect('/transaksi');
    })->name('bank.finish');

    Route::POST('/bank/store', 'TransaksiController@submitBank')->name('bank.store');
    Route::post('/notification/handler', 'TransaksiController@notificationHandler')->name('notification.handler');
    // ============================= akhir midtrans =======================

});
// ========================== akhir transaksi =====================
